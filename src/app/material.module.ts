import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatLuxonDateModule } from "@angular/material-luxon-adapter";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatButtonModule } from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import { MatDialogModule } from "@angular/material/dialog";
import { MatTableModule } from "@angular/material/table";
import { MatInputModule } from "@angular/material/input";
import { MatChipsModule } from "@angular/material/chips";
import { MatRadioModule } from "@angular/material/radio";
import { MatMenuModule } from "@angular/material/menu";
import { MatListModule } from "@angular/material/list";
import { MatIconModule } from "@angular/material/icon";
import { MatTabsModule } from "@angular/material/tabs";
import { NgModule } from "@angular/core";

@NgModule({
	exports: [
		MatProgressSpinnerModule,
		MatListModule,
		MatIconModule,
		MatTooltipModule,
		MatSidenavModule,
		MatSelectModule,
		MatSnackBarModule,
		MatButtonModule,
		MatTableModule,
		MatFormFieldModule,
		MatInputModule,
		MatExpansionModule,
		MatPaginatorModule,
		MatDialogModule,
		MatSlideToggleModule,
		MatDatepickerModule,
		MatLuxonDateModule,
		MatChipsModule,
		MatCheckboxModule,
		MatMenuModule,
		MatTabsModule,
		MatButtonToggleModule,
		MatRadioModule,
	],
})
export class MaterialImportsModule {}
