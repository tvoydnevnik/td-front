import { EnvironmentProviders, Provider } from "@angular/core";
import { LUXON_ISO_TIME_OPTS } from "@td/constants/injection-tokens";

export const constantProviders: (Provider | EnvironmentProviders)[] = [
	{
		provide: LUXON_ISO_TIME_OPTS,
		useValue: {
			suppressMilliseconds: true,
			suppressSeconds: true,
			includeOffset: false,
		},
	},
];
