import { HttpHeaders } from "@angular/common/http";
import { TD } from "@td";

export interface IHttpError {
	error: TD.Types.IHttpResponse<null>;
	headers: HttpHeaders;
	message: string;
	name: string;
	ok: boolean;
	status: number;
	statusText: string;
	url: string;
}
