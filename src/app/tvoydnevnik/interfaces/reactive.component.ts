import { Subject } from "rxjs";
import { Component, OnDestroy } from "@angular/core";

@Component({
	template: "",
})
export abstract class ReactiveComponent implements OnDestroy {
	protected destroy$ = new Subject<void>();

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}
}
