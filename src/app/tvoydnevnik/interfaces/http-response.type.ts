import { Observable } from "rxjs";
import { Types } from "td-types";

export type HttpResponse<T> = Observable<Types.IHttpResponse<T>>;
