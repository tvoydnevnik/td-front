import { DateTime } from "luxon";
import { TD } from "@td";

const DateDisplay = (isoDate: string, format: string) =>
	DateTime.fromISO(isoDate).toFormat(format);

export const UserFullNameDisplay = (user?: TD.Types.IUser) =>
	user
		? `${user.last_name} ${user.first_name}` +
		  (user.patronymic ? ` ${user.patronymic}` : "")
		: "";

export const FormTitleDisplay = (form?: TD.Types.IForm) =>
	form ? `${form.grade} \"${form.letter}\"` : "";

export const TeacherDisplay = (teacher?: TD.Types.ITeacher) =>
	teacher ? `${UserFullNameDisplay(teacher.user)}` : "";

export const HeadmasterDisplay = (headmaster?: TD.Types.IHeadmaster) =>
	headmaster ? `${UserFullNameDisplay(headmaster.user)}` : "";

export const TeacherChipDisplay = (teacher?: TD.Types.ITeacher) =>
	teacher
		? `${teacher?.user?.last_name} ${teacher?.user?.first_name[0]}. ${
				teacher?.user?.patronymic ? `${teacher.user.patronymic[0]}.` : ""
		  }`
		: "";

export const TeacherCompare = (
	teacher?: TD.Types.ITeacher,
	other?: TD.Types.ITeacher,
) => teacher?.id === other?.id;

export const StudentCompare = (
	student?: TD.Types.IStudent,
	other?: TD.Types.IStudent,
) => student?.id === other?.id;

export const StudentDisplay = (student?: TD.Types.IStudent) =>
	student ? `${UserFullNameDisplay(student.user)}` : "";

export const StudentChipDisplay = (student?: TD.Types.IStudent) =>
	student ? `${student?.user?.last_name} ${student?.user?.first_name[0]}.` : "";

export const FormDisplay = (form?: TD.Types.IForm) =>
	form ? `${FormTitleDisplay(form)} (${TeacherDisplay(form.master)})` : "";

export const FormSubgroupDisplay = (subgroup?: TD.Types.IFormSubgroup) =>
	subgroup ? `${subgroup.name}` : "";

export const QuarterDisplay = (quarter?: TD.Types.IQuarter) =>
	quarter
		? `${DateDisplay(
				quarter.start_date.toString(),
				"MMMM dd, yyyy",
		  )} - ${DateDisplay(quarter.end_date.toString(), "MMMM dd, yyyy")}`
		: "";

export const LessonDisplay = (lesson?: TD.Types.ILesson) =>
	lesson ? `${lesson.name}` : "";

export const FormLessonDisplay = (l?: TD.Types.IFormLesson) =>
	l
		? `${LessonDisplay(l.lesson)}${
				l.form_subgroup ? ` (${FormSubgroupDisplay(l.form_subgroup)})` : ""
		  }`
		: "";

export const BellsScheduleGroupDisplay = (schedule?: TD.Types.IBellsSchedule) =>
	schedule ? `${schedule.name}` : "";
