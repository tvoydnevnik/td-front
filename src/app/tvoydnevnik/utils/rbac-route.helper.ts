import { rolesGuardFn } from "@shared/guards/roles.guard";

import { TD } from "@td";
import { authGuardFn } from "@shared/guards/auth.guard";

export const RBAC_Route = (minimal_role: TD.Types.UserRole) => ({
	data: { minimal_role },
	canActivate: [authGuardFn, rolesGuardFn],
	canLoad: [authGuardFn, rolesGuardFn],
});
