import { TD } from "@td";
import { DateTime } from "luxon";

export const GetLessonDate = (entry?: TD.Types.ITimetableEntry): string => {
	if (!entry) return "";

	const d = DateTime.fromISO(<any>entry.quarter_week?.start_date);
	return d.plus({ days: entry.weekday }).toLocaleString(DateTime.DATE_FULL);
};

export const GetAriaLabelForDiaryPopover = (
	entry?: TD.Types.ITimetableEntry,
	student?: TD.Types.IStudent,
): string => {
	let msg = "Grade student";

	if (student) msg = `Grade ${TD.UserFullNameDisplay(student.user)}`;

	if (entry?.form_lesson?.lesson)
		msg += `, subject ${entry.form_lesson.lesson.name}`;

	if (entry?.quarter_week) msg += ` that took place ${GetLessonDate(entry)}`;

	return msg;
};

export const GetAriaLabelForTimetablePopover = (
	entry?: TD.Types.ITimetableEntry,
): string => {
	let msg = "Edit lesson details";

	if (entry?.form_lesson?.lesson)
		msg += ` for ${entry.form_lesson.lesson.name}`;

	if (entry?.quarter_week) msg += ` that took place ${GetLessonDate(entry)}`;

	return msg;
};
