import { IHttpError } from "@td/interfaces/http-error.interface";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
	catchError,
	forkJoin,
	map,
	mergeMap,
	of,
	OperatorFunction,
	pipe,
} from "rxjs";

export const HandleCRUDRequest = <T>(
	snackbar: MatSnackBar,
	doNotRedirect = false,
): OperatorFunction<T, T> =>
	pipe(
		mergeMap((v) =>
			forkJoin([
				snackbar
					.open(
						`Success! 
					${doNotRedirect ? "" : " Redirecting back in 3 seconds..."}`,
						doNotRedirect ? "OK" : "Back",
						{
							duration: 3000,
						},
					)
					.afterDismissed(),
				of(v),
			]),
		),
		map(([, v]) => v),
		HandleHTTPError(snackbar),
	);

export const HandleHTTPError = <T>(
	snackbar: MatSnackBar,
): OperatorFunction<T, T> =>
	pipe(
		catchError((err: IHttpError) =>
			snackbar
				.open(
					`${
						err?.error?.message ?? err
					}. Contact an administrator or try again later.`,
					"OK",
				)
				.afterDismissed()
				.pipe(
					map(() => {
						throw new Error(err?.error?.message ?? err);
					}),
				),
		),
	);
