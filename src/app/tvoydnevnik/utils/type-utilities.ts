/**
 * Modifies given type with Modifier argument. Also omits `id` property by default.
 */
export type Modify<Type, Modifier> = Omit<Type, keyof Modifier | "id"> &
	Modifier;
