import { InjectionToken } from "@angular/core";
import { ToISOTimeOptions } from "luxon";

export const LUXON_ISO_TIME_OPTS = new InjectionToken<ToISOTimeOptions>(
	"LUXON_TO_ISO_TIME_OPTS",
);
