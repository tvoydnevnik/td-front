import { HttpService, IHttpOptions } from "@td/api/http.service";
import { inject, Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "@env";
import { TD } from "@td";

// TODO: Use ApiClient instead of cluttered services altogether
@Injectable({
	providedIn: "root",
})
export class ApiClientService {
	private readonly _http_client = inject(HttpClient);
	private readonly _http = new HttpService(this._http_client);

	constructor() {}

	public Get<ResponseT = any, ParamsT extends object = {}>(
		path: string,
		options?: HttpOptionsWithRequest<ParamsT>,
	): TD.HttpResponse<ResponseT> {
		return this._http.get<ResponseT>(
			this._ResolvePath(path),
			this._ResolveOptions(options),
		);
	}

	public Post<ResponseT = any, PayloadT = any, ParamsT extends object = {}>(
		path: string,
		payload: PayloadT,
		options?: HttpOptionsWithRequest<ParamsT>,
	): TD.HttpResponse<ResponseT> {
		return this._http.post<ResponseT>(
			this._ResolvePath(path),
			payload,
			this._ResolveOptions(options),
		);
	}

	public Patch<ResponseT = any, PayloadT = any, ParamsT extends object = {}>(
		path: string,
		payload: PayloadT,
		options?: HttpOptionsWithRequest<ParamsT>,
	): TD.HttpResponse<ResponseT> {
		return this._http.patch<ResponseT>(
			this._ResolvePath(path),
			payload,
			this._ResolveOptions(options),
		);
	}

	public Put<ResponseT = any, PayloadT = any, ParamsT extends object = {}>(
		path: string,
		payload: PayloadT,
		options?: HttpOptionsWithRequest<ParamsT>,
	): TD.HttpResponse<ResponseT> {
		return this._http.put<ResponseT>(
			this._ResolvePath(path),
			payload,
			this._ResolveOptions(options),
		);
	}

	public Delete<ResponseT = any, ParamsT extends object = {}>(
		path: string,
		options?: HttpOptionsWithRequest<ParamsT>,
	): TD.HttpResponse<ResponseT> {
		return this._http.delete<ResponseT>(
			this._ResolvePath(path),
			this._ResolveOptions(options),
		);
	}

	private _ResolveOptions(
		opts?: HttpOptionsWithRequest<object>,
	): IHttpOptions | undefined {
		if (!opts?.request) return opts;

		return {
			...opts,
			params: {
				...opts.params,
				request: JSON.stringify(opts.request),
			},
		};
	}

	private _ResolvePath(path: string): string {
		return `${environment.apiUrl}/${
			path.startsWith("/") ? path.substring(1) : path
		}`;
	}
}

export type HttpOptionsWithRequest<T extends object> = IHttpOptions & {
	request?: T;
};
