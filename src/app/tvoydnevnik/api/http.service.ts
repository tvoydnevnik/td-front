import { Injectable } from "@angular/core";
import {
	HttpClient,
	HttpContext,
	HttpHeaders,
	HttpParams,
} from "@angular/common/http";
import { TD } from "@td";

@Injectable({
	providedIn: "root",
})
export class HttpService {
	constructor(private readonly http: HttpClient) {}

	get<T>(url: string, options?: IHttpOptions): TD.HttpResponse<T> {
		return this.http.get<TD.Types.IHttpResponse<T>>(url, options);
	}

	post<T>(url: string, body: any, options?: IHttpOptions): TD.HttpResponse<T> {
		return this.http.post<TD.Types.IHttpResponse<T>>(url, body, options);
	}

	patch<T>(url: string, body: any, options?: IHttpOptions): TD.HttpResponse<T> {
		return this.http.patch<TD.Types.IHttpResponse<T>>(url, body, options);
	}

	put<T>(url: string, body: any, options?: IHttpOptions): TD.HttpResponse<T> {
		return this.http.put<TD.Types.IHttpResponse<T>>(url, body, options);
	}

	delete<T>(url: string, options?: IHttpOptions): TD.HttpResponse<T> {
		return this.http.delete<TD.Types.IHttpResponse<T>>(url, options);
	}
}

export interface IHttpOptions {
	headers?: IHttpHeaders;
	context?: HttpContext;
	params?: IHttpParams;
	withCredentials?: boolean;
}

export type IHttpHeaders =
	| HttpHeaders
	| { [header: string]: string | string[] };

export type ParamValue =
	| string
	| number
	| boolean
	| ReadonlyArray<string | number | boolean>;

export type IHttpParams = HttpParams | { [param: string]: ParamValue };
