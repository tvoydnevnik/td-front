import { CredentialsInterceptor } from "@shared/interceptors/credentials.interceptor";
import { MAT_AUTOCOMPLETE_DEFAULT_OPTIONS } from "@angular/material/autocomplete";
import { MAT_LUXON_DATE_ADAPTER_OPTIONS } from "@angular/material-luxon-adapter";
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
import { popperVariation, provideTippyConfig } from "@ngneat/helipopper";
import { HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { EnvironmentProviders, Provider } from "@angular/core";
import {
	FullscreenOverlayContainer,
	OverlayContainer,
} from "@angular/cdk/overlay";
import { TD } from "@td";

export const appProviders: (Provider | EnvironmentProviders)[] = [
	{
		provide: HTTP_INTERCEPTORS,
		useClass: CredentialsInterceptor,
		multi: true,
	},
	{
		provide: OverlayContainer,
		useClass: FullscreenOverlayContainer,
	},
	{
		provide: TD.HttpService,
		useClass: TD.HttpService,
		deps: [HttpClient],
	},
	{
		provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
		useValue: {
			appearance: "outline",
		},
	},
	{
		provide: MAT_AUTOCOMPLETE_DEFAULT_OPTIONS,
		useValue: {
			autoActiveFirstOption: true,
		},
	},
	{
		provide: MAT_LUXON_DATE_ADAPTER_OPTIONS,
		useValue: { useUtc: true },
	},
	provideTippyConfig({
		defaultVariation: "popper",
		variations: {
			popper: popperVariation,
		},
		theme: "material",
	}),
];
