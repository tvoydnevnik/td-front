import { LoadingComponent, OverlayService } from "@td-lib";
import { AuthService } from "@services/auth.service";
import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
	constructor(
		private readonly overlayService: OverlayService,
		private readonly authService: AuthService,
	) {}

	ngOnInit() {
		this.overlayService.createOverlay(LoadingComponent);
		this.authService.ValidateAuth().subscribe({
			next: () => this.overlayService.clearOverlay(),
			error: () => this.overlayService.clearOverlay(),
		});
	}
}
