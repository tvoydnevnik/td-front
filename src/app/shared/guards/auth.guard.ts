import { CanActivateFn, CanMatchFn, Router } from "@angular/router";
import { inject } from "@angular/core";

import { AuthService } from "@services/auth.service";

import { map } from "rxjs";

export const authGuardFn: CanActivateFn | CanMatchFn = () => {
	const authService = inject(AuthService);
	const router = inject(Router);

	return authService
		.IsAuth()
		.pipe(map((isAuth) => (isAuth ? true : router.parseUrl("/login"))));
};
