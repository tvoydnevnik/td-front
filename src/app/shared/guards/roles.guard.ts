import { MatSnackBar } from "@angular/material/snack-bar";
import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivateFn } from "@angular/router";

import { RouterService } from "@services/router.service";
import { UsersService } from "@services/users.service";

import { map, tap } from "rxjs";

export const rolesGuardFn: CanActivateFn = (route: ActivatedRouteSnapshot) => {
	const usersService = inject(UsersService);
	const router = inject(RouterService);
	const snackbar = inject(MatSnackBar);

	const { minimal_role } = route.data;
	if (!minimal_role) return true;

	return usersService.User.pipe(
		map((user) => (user ? user.role >= minimal_role : false)),
		tap((res) => {
			if (!res) {
				router.NavigateBack();
				snackbar.open("Access denied!", "OK", { duration: 3000 });
			}
		}),
	);
};
