import { UsersService } from "./users.service";
import { Injectable } from "@angular/core";
import { environment } from "@env";
import {
	catchError,
	map,
	mergeMap,
	Observable,
	of,
	ReplaySubject,
	tap,
} from "rxjs";
import { TD } from "@td";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	private _isAuth$ = new ReplaySubject<boolean>(1);

	constructor(
		private readonly http: TD.HttpService,
		private readonly usersService: UsersService,
	) {}

	public IsAuth(): Observable<boolean> {
		return this._isAuth$.asObservable();
	}

	public ValidateAuth(): Observable<boolean> {
		return this._ValidateAuth();
	}

	public ValidateRoles(
		minimal_role: TD.Types.UserRole,
	): Observable<{ user: TD.Types.IUser | undefined; has_access: boolean }> {
		return this.usersService.User.pipe(
			map((u) => ({
				user: u,
				has_access: u ? u.role >= minimal_role : false,
			})),
		);
	}

	Login(identifier: string, password: string): TD.HttpResponse<TD.Types.IUser> {
		return this.http
			.post<TD.Types.IUser>(`${environment.apiUrl}/auth/login`, {
				identifier,
				password,
			})
			.pipe(mergeMap(() => this._SaveLogin()));
	}

	Register(payload: {
		code: string;
		username: string;
		email: string;
		password: string;
	}): TD.HttpResponse<TD.Types.IUser> {
		return this.http.post<TD.Types.IUser>(
			`${environment.apiUrl}/auth/register`,
			payload,
		);
	}

	Logout(): TD.HttpResponse<boolean> {
		return this.http
			.post<boolean>(`${environment.apiUrl}/auth/logout`, null)
			.pipe(tap(() => this._ForgetLogin()));
	}

	private _SaveLogin(): TD.HttpResponse<TD.Types.IUser> {
		return this.usersService.GetMe().pipe(
			map((res) => {
				this._isAuth$.next(true);
				return res;
			}),
		);
	}

	private _ForgetLogin(): void {
		this.usersService.SetUser(undefined);
		this._isAuth$.next(false);
	}

	private _ValidateAuth(): Observable<boolean> {
		return this.http.get<boolean>(`${environment.apiUrl}/auth/validate`).pipe(
			mergeMap((response) => {
				this._isAuth$.next(response.data!);
				if (!response.data) return of(undefined);

				return this.usersService.GetMe();
			}),
			map((user) => !!user),
			catchError(() => {
				this._isAuth$.next(false);
				return of(false);
			}),
		);
	}
}
