import { NavigationEnd, Router } from "@angular/router";
import { Injectable, OnDestroy, Optional, SkipSelf } from "@angular/core";
import { filter, map, Subscription } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class NavigationHistoryService implements OnDestroy {
	private _sub$ = new Subscription();

	private _currentUrl = "";
	private _previousUrl = "";

	constructor(
		router: Router,
		@Optional() @SkipSelf() parentService?: NavigationHistoryService,
	) {
		if (parentService) {
			throw new Error(
				"NavigationHistoryService is already loaded. Singleton services should only be instantiated once.",
			);
		}

		this._sub$.add(
			router.events
				.pipe(
					filter((e) => e instanceof NavigationEnd),
					map((e) => e as NavigationEnd),
				)
				.subscribe((e: NavigationEnd) => {
					this._previousUrl = this._currentUrl;
					this._currentUrl = e.url;
				}),
		);
	}

	ngOnDestroy() {
		this._sub$.unsubscribe();
	}

	get PreviousUrl(): string {
		return this._previousUrl;
	}

	get CurrentUrl(): string {
		return this._currentUrl;
	}
}
