import { Observable, ReplaySubject, tap } from "rxjs";
import { Injectable } from "@angular/core";
import { environment } from "@env";
import { TD } from "@td";

@Injectable({
	providedIn: "root",
})
export class UsersService {
	private user$ = new ReplaySubject<TD.Types.IUser | undefined>(1);

	constructor(private readonly http: TD.HttpService) {}

	get User(): Observable<TD.Types.IUser | undefined> {
		return this.user$.asObservable();
	}

	SetUser(value?: TD.Types.IUser): void {
		this.user$.next(value);
	}

	GetMe(): TD.HttpResponse<TD.Types.IUser> {
		return this.http
			.get<TD.Types.IUser>(`${environment.apiUrl}/users/me`)
			.pipe(tap((response) => this.SetUser(response.data)));
	}

	GetMySessions(): TD.HttpResponse<TD.Types.SessionData[]> {
		return this.http.get<TD.Types.SessionData[]>(
			`${environment.apiUrl}/auth/sessions`,
		);
	}

	LogoutSession(sid: string): TD.HttpResponse<boolean> {
		return this.http.post<boolean>(
			`${environment.apiUrl}/auth/logout/${sid}`,
			null,
		);
	}
}
