import { NavigationHistoryService } from "@services/navigation-history.service";
import { NavigationBehaviorOptions, Router, UrlTree } from "@angular/router";
import { Injectable } from "@angular/core";
import { from, Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class RouterService {
	constructor(
		private readonly navigationHistory: NavigationHistoryService,
		private readonly router: Router,
	) {}

	get NativeRouter(): Router {
		return this.router;
	}

	NavigateBack(extras?: NavigationBehaviorOptions): Observable<boolean> {
		return this.Navigate(this.navigationHistory.PreviousUrl, extras);
	}

	Navigate(
		destination: string | UrlTree | any[],
		extras?: NavigationBehaviorOptions,
	): Observable<boolean> {
		if (Array.isArray(destination)) {
			return this._NavigateByCommands(destination, extras);
		} else {
			return this._NavigateByUrl(destination, extras);
		}
	}

	private _NavigateByUrl(
		url: string | UrlTree,
		extras?: NavigationBehaviorOptions,
	) {
		return from(this.router.navigateByUrl(url, extras));
	}

	private _NavigateByCommands(
		commands: any[],
		extras?: NavigationBehaviorOptions,
	) {
		return from(this.router.navigate(commands, extras));
	}
}
