import { Pipe, PipeTransform } from "@angular/core";
import { TD } from "@td";

@Pipe({
	standalone: true,
	name: "userDisplayName",
})
export class UserDisplayNamePipe implements PipeTransform {
	transform(value?: TD.Types.IUser | TD.Types.IUser[]): string {
		if (Array.isArray(value)) {
			return value.map((u) => TD.UserFullNameDisplay(u)).join(", ");
		} else {
			return TD.UserFullNameDisplay(value);
		}
	}
}
