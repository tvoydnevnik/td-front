import { Pipe, PipeTransform } from "@angular/core";
import { TD } from "@td";

@Pipe({
	standalone: true,
	name: "formDisplayName",
})
export class FormDisplayNamePipe implements PipeTransform {
	transform(value?: TD.Types.IForm | TD.Types.IForm[]): string {
		if (Array.isArray(value)) {
			return value.map((f) => TD.FormTitleDisplay(f)).join(", ");
		} else {
			return TD.FormTitleDisplay(value);
		}
	}
}
