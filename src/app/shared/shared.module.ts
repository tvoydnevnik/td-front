import { FormDisplayNamePipe } from "@shared/pipes/form-display-name.pipe";
import { UserDisplayNamePipe } from "./pipes/user-display-name.pipe";
import { MaterialImportsModule } from "../material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { TippyDirective } from "@ngneat/helipopper";
import { NgModule } from "@angular/core";
import {
	ChipsSelectAutocompleteComponent,
	ItemEntryContentDirective,
	ItemEntryDescriptionDirective,
	ItemEntryTitleDirective,
	ItemSelectAutocompleteComponent,
	ItemsListComponent,
	TransformByOptionsPipe,
	StayOnPageComponent,
	TimetableDisplayComponent,
	TimePipe,
	TdIfDirective,
	TdLoadDirective,
	EnumPipe,
} from "@td-lib";

const MODULES = [MaterialImportsModule, ReactiveFormsModule];

const COMPONENTS = [
	ItemsListComponent,
	ItemSelectAutocompleteComponent,
	ChipsSelectAutocompleteComponent,
	StayOnPageComponent,
	TimetableDisplayComponent,
];

const DIRECTIVES = [
	ItemEntryTitleDirective,
	ItemEntryDescriptionDirective,
	ItemEntryContentDirective,
	TdIfDirective,
	TdLoadDirective,
	TippyDirective,
];

const PIPES = [
	TransformByOptionsPipe,
	UserDisplayNamePipe,
	FormDisplayNamePipe,
	TimePipe,
	EnumPipe,
];

@NgModule({
	imports: [...MODULES, ...COMPONENTS, ...DIRECTIVES, ...PIPES],
	exports: [...MODULES, ...COMPONENTS, ...DIRECTIVES, ...PIPES],
})
export class SharedModule {}
