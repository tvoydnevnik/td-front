import { MenuGroupHeaderComponent } from "../menu-group-header/menu-group-header.component";
import { MatAnchor, MatButtonModule } from "@angular/material/button";
import { UsersService } from "@services/users.service";
import { AuthService } from "@services/auth.service";
import { MenuLayout } from "../../menu-layout";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { A11yModule } from "@angular/cdk/a11y";
import { map, Observable, of } from "rxjs";
import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	Input,
	ViewChild,
} from "@angular/core";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		MenuGroupHeaderComponent,
		RouterModule,
		MatButtonModule,
		A11yModule,
	],
	selector: "app-menu",
	templateUrl: "./menu.component.html",
	styleUrls: ["./menu.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent implements AfterViewInit {
	@Input() layout!: MenuLayout;

	@ViewChild("menu") private readonly menu!: ElementRef<HTMLDivElement>;
	@ViewChild("first") private readonly first!: MatAnchor;
	private _menuOpen = false;

	constructor(
		public readonly authService: AuthService,
		public readonly usersService: UsersService,
		private readonly changeRef: ChangeDetectorRef,
	) {}

	public IsOpen = (): boolean => {
		return this._menuOpen;
	};

	public IsClosed = (): boolean => {
		return !this.IsOpen();
	};

	public ToggleMenu = (): void => {
		this._menuOpen = !this._menuOpen;
		this.changeRef.detectChanges();
		if (this._menuOpen) this.first.focus();
	};

	protected ValidateRoles(
		minimal_role?: TD.Types.UserRole,
	): Observable<boolean> {
		if (!minimal_role) return of(true);

		return this.usersService.User.pipe(
			map((u) => (u ? u.role >= minimal_role : false)),
		);
	}

	ngAfterViewInit() {
		// trigger menu of mouse click outside menu container
		this.menu.nativeElement.addEventListener("click", (e) => {
			const t = e.target as HTMLElement;
			if (t.classList.contains("menu")) {
				this.ToggleMenu();
			}
		});

		// trigger menu on Escape pressed or menu entry chosen
		this.menu.nativeElement.addEventListener("keydown", (e) => {
			switch (e.key) {
				case "Escape":
				case "Enter":
					this.ToggleMenu();
					break;
			}
		});
	}
}
