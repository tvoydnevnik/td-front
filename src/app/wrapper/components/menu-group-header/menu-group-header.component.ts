import { Component, Input } from "@angular/core";
import { CommonModule } from "@angular/common";

@Component({
	selector: "td-menu-group-header",
	standalone: true,
	imports: [CommonModule],
	templateUrl: "./menu-group-header.component.html",
	styleUrls: ["./menu-group-header.component.scss"],
})
export class MenuGroupHeaderComponent {
	@Input() title: string = "Group";
}
