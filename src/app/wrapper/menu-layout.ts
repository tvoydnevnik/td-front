import { TD } from "@td";

interface Link {
	title: string;
	url: string;
	minimal_role?: TD.Types.UserRole;
}

interface MenuGroupLayout {
	title: string;
	links: Link[];
}

export type MenuLayout = MenuGroupLayout[];
