import { RouterService } from "@services/router.service";
import { MenuLayout } from "./menu-layout";
import {
	AfterViewInit,
	Component,
	ElementRef,
	OnDestroy,
	ViewChild,
} from "@angular/core";
import {
	filter,
	fromEvent,
	Observable,
	scan,
	Subscription,
	tap,
	timeInterval,
} from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-wrapper",
	templateUrl: "./wrapper.component.html",
	styleUrls: ["./wrapper.component.scss"],
})
export class WrapperComponent implements AfterViewInit, OnDestroy {
	@ViewChild("hearts") hearts!: ElementRef<HTMLParagraphElement>;
	clickListener?: Observable<any>;
	clickSubscription = new Subscription();

	menuLayout: MenuLayout = [
		{
			title: "School",
			links: [
				{ title: "About", url: "/school/about" },
				{ title: "Forms", url: "/school/forms" },
				{
					title: "Management",
					url: "/school/admin/management",
					minimal_role: TD.Types.UserRole.HEADMASTER,
				},
			],
		},
		{
			title: "eDiary",
			links: [
				{ title: "Timetable", url: "/timetable" },
				{ title: "Diary", url: "/diary" },
			],
		},
	];

	constructor(private readonly router: RouterService) {}

	ngAfterViewInit() {
		this._setupClickListener();
	}

	ngOnDestroy() {
		this.clickSubscription?.unsubscribe();
	}

	private _activate(): void {
		// This is intentional. Should not be purged in production.
		console.log("[Developers]: I am an eastern bunny!");
		this.router.Navigate("/easteregg", { state: { allowed: true } });
	}

	private _setupClickListener(): void {
		// Listen to clicks and filter 10 continuous ones to activate
		this.clickListener = fromEvent(this.hearts.nativeElement, "click").pipe(
			timeInterval(),
			scan(
				(acc, value) => (acc === 0 ? ++acc : value.interval < 450 ? ++acc : 0),
				0,
			),
			filter((val) => val === 10),
			tap(() => this._activate()),
		);

		this.clickSubscription?.add(this.clickListener.subscribe());
	}
}
