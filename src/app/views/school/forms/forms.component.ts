import { ApiClientService } from "@td/api/api-client.service";
import { DisplayableForms } from "./displayable-forms";
import { Component, inject } from "@angular/core";

@Component({
	selector: "app-forms",
	templateUrl: "./forms.component.html",
	styleUrls: ["./forms.component.scss"],
})
export class FormsComponent {
	// Injected props
	private readonly apiClient = inject(ApiClientService);

	forms?: DisplayableForms;

	constructor() {
		this.apiClient
			.Get("/forms")
			.subscribe((r) => (this.forms = new DisplayableForms(r.data || [])));
	}
}
