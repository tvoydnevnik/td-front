import { TD } from "@td";

type FormsMap = Map<string, TD.Types.IForm[]>;

export class DisplayableForms {
	data: FormsMap;

	constructor(payload: TD.Types.IForm[]) {
		this.data = new Map();
		payload.forEach((form) => {
			const grade = form.grade.toString();
			if (!this.data.has(grade)) this.data.set(grade, []);

			this.data.get(grade)!.push(form);
		});

		// Sort alphabetically by form letter.
		this.data.forEach((grade) =>
			grade.sort((a, b) => a.letter.localeCompare(b.letter)),
		);
	}
}
