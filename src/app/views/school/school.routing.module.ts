import { SchoolAboutComponent } from "./about/school-about.component";
import { TeacherComponent } from "./teacher/teacher.component";
import { StudentComponent } from "./student/student.component";
import { FormsComponent } from "./forms/forms.component";
import { RouterModule, Routes } from "@angular/router";
import { FormComponent } from "./form/form.component";
import { NgModule } from "@angular/core";
import { TD } from "@td";

const routes: Routes = [
	{
		path: "about",
		component: SchoolAboutComponent,
	},
	{
		path: "admin",
		loadChildren: () =>
			import("./school-admin/school-admin.module").then(
				(m) => m.SchoolAdminModule,
			),
		...TD.RBAC_Route(TD.Types.UserRole.ADMIN),
	},
	{
		path: "forms",
		component: FormsComponent,
	},
	{
		path: "form/:id",
		component: FormComponent,
	},
	{
		path: "student/:id",
		component: StudentComponent,
	},
	{
		path: "teacher/:id",
		component: TeacherComponent,
	},
	{
		path: "timetable-wizard/:form_id",
		...TD.RBAC_Route(TD.Types.UserRole.TEACHER),
		loadComponent: () =>
			import("./timetable-wizard/timetable-wizard.component"),
	},
	{
		path: "",
		pathMatch: "full",
		redirectTo: "about",
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SchoolRoutingModule {}
