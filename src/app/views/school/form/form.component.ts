import { FormTimetableTabComponent } from "./tabs/form-timetable-tab/form-timetable-tab.component";
import { FormStudentsTabComponent } from "./tabs/form-students-tab/form-students-tab.component";
import { FormDiaryTabComponent } from "./tabs/form-diary-tab/form-diary-tab.component";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { ApiClientService } from "@td/api/api-client.service";
import { ActivatedRoute, RouterLink } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { SharedModule } from "@shared/shared.module";
import { AuthService } from "@services/auth.service";
import { Component, inject } from "@angular/core";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		SharedModule,
		FormDiaryTabComponent,
		FormStudentsTabComponent,
		FormTimetableTabComponent,
		RouterLink,
		NgOptimizedImage,
	],
	selector: "app-form",
	templateUrl: "./form.component.html",
	styleUrls: ["./form.component.scss"],
})
export class FormComponent {
	id?: string | null;
	form?: TD.Types.IForm;

	public readonly authService = inject(AuthService);
	private readonly apiClient = inject(ApiClientService);

	constructor(
		private readonly activatedRoute: ActivatedRoute,
		private readonly snackbar: MatSnackBar,
		private readonly routerService: RouterService,
	) {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((map) => {
					this.id = map.get("id");
					return this.apiClient.Get(`/forms/${this.id}`);
				}),
			)
			.subscribe({
				next: (r) => (this.form = r.data),
				error: (e: TD.IHttpError) =>
					this.snackbar
						.open(e.error.message, "Back")
						.afterDismissed()
						.subscribe(() => this.routerService.NavigateBack()),
			});
	}

	protected readonly TD = TD;
}
