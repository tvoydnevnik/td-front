import { FormQuarterDiaryComponent } from "./components/form-quarter-diary/form-quarter-diary.component";
import { FormYearDiaryComponent } from "./components/form-year-diary/form-year-diary.component";
import { AfterViewInit, Component, inject, Input } from "@angular/core";
import { ApiClientService } from "@td/api/api-client.service";
import { LoadingComponent, OverlayService } from "@td-lib";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import { forkJoin } from "rxjs";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		SharedModule,
		FormQuarterDiaryComponent,
		FormYearDiaryComponent,
	],
	selector: "app-form-diary-tab",
	templateUrl: "./form-diary-tab.component.html",
	styleUrls: ["./form-diary-tab.component.scss"],
})
export class FormDiaryTabComponent implements AfterViewInit {
	// Input props
	@Input() form!: TD.Types.IForm;

	// Injected props
	private readonly apiClient = inject(ApiClientService);
	private readonly overlay = inject(OverlayService);
	private readonly snackbar = inject(MatSnackBar);

	quarters?: TD.Types.IQuarter[];
	lessonSelectConfig?: TD.ItemSelectAutocompleteConfig<TD.Types.IFormLesson>;
	lesson?: TD.Types.IFormLesson | null;

	ngAfterViewInit() {
		this.overlay.createOverlay(LoadingComponent);

		forkJoin({
			quarters: this.apiClient.Get<TD.Types.IQuarter[]>(
				"/quarters/current-year",
			),
			lessons: this.apiClient.Get<TD.Types.IFormLesson[]>(
				`/forms/${this.form.id}/lessons`,
			),
		})
			.pipe(TD.HandleHTTPError(this.snackbar))
			.subscribe({
				next: ({ quarters, lessons }) => {
					this.quarters = quarters.data;
					this.lessonSelectConfig = new TD.ItemSelectAutocompleteConfig({
						label: "Subject",
						options: lessons.data ?? [],
						displayWith: TD.FormLessonDisplay,
					});

					this.overlay.clearOverlay();
				},
				error: () => this.overlay.clearOverlay(),
			});
	}
}
