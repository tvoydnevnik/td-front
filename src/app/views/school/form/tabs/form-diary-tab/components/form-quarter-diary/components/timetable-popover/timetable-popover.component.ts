import { ControlContainer, FormBuilder } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { TippyDirective } from "@ngneat/helipopper";
import { CommonModule } from "@angular/common";
import {
	QuarterDiaryFormGroup,
	TimetablePayloadFormGroup,
} from "../../form-quarter-diary.component";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
} from "@angular/core";
import { TD } from "@td";

@Component({
	selector: "app-timetable-popover",
	standalone: true,
	imports: [CommonModule, SharedModule],
	templateUrl: "./timetable-popover.component.html",
	styleUrls: ["./timetable-popover.component.scss"],
})
export class TimetablePopoverComponent implements AfterViewInit {
	// Input props
	@Input() timetable_entry?: TD.Types.ITimetableEntry;
	@Input() tippy?: TippyDirective;

	// Injected props
	private readonly _controlContainer = inject(ControlContainer);
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly fb = inject(FormBuilder);

	private externalFormGroup?: QuarterDiaryFormGroup;
	timetableOptionsForm: TimetablePayloadFormGroup = this.fb.group({
		timetable_entry_id: this.fb.nonNullable.control<number>(-1),
		homework: this.fb.nonNullable.control<string | undefined>(""),
		lesson_topic: this.fb.nonNullable.control<string | undefined>(""),
	});

	ngAfterViewInit() {
		if (!this.timetable_entry) throw Error("Timetable entry not provided!");

		this._SetupFormControl();
	}

	TriggerChanges(): void {
		this.changeRef.detectChanges();
	}

	Hide(): void {
		this.tippy?.hide();
	}

	private _SetupFormControl(): void {
		if (!this._controlContainer) return;
		if (!this._controlContainer.control) return;

		this.externalFormGroup = <QuarterDiaryFormGroup>(
			this._controlContainer.control
		);

		let existingOptionsForm =
			this.externalFormGroup.controls.timetable.controls.find(
				({ value: v }) => v.timetable_entry_id === this.timetable_entry?.id,
			);

		if (!existingOptionsForm) {
			this.externalFormGroup.controls.timetable.push(
				this.fb.group({
					timetable_entry_id: this.fb.nonNullable.control<number>(
						this.timetable_entry!.id,
					),
					lesson_topic: this.fb.nonNullable.control<string | undefined>(""),
					homework: this.fb.nonNullable.control<string | undefined>(""),
				}),
			);

			existingOptionsForm = this.externalFormGroup.controls.timetable.at(-1);
		}

		this.timetableOptionsForm = existingOptionsForm;
	}

	protected readonly TD = TD;
}
