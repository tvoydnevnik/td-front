import { FormDiaryPopoverToggleComponent } from "../form-diary-popover-toggle/form-diary-popover-toggle.component";
import { TimetablePopoverComponent } from "./components/timetable-popover/timetable-popover.component";
import { DiaryPopoverComponent } from "./components/diary-popover/diary-popover.component";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ApiClientService } from "@td/api/api-client.service";
import { MatTab, MatTabGroup } from "@angular/material/tabs";
import { LoadingComponent, OverlayService } from "@td-lib";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UsersService } from "@services/users.service";
import { forkJoin, mergeMap, takeUntil } from "rxjs";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import { DateTime } from "luxon";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
	OnChanges,
} from "@angular/core";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		SharedModule,
		DiaryPopoverComponent,
		TimetablePopoverComponent,
		FormDiaryPopoverToggleComponent,
	],
	selector: "app-form-quarter-diary",
	templateUrl: "./form-quarter-diary.component.html",
	styleUrls: ["./form-quarter-diary.component.scss"],
})
export class FormQuarterDiaryComponent
	extends ReactiveComponent
	implements AfterViewInit, OnChanges
{
	// Injected props
	public readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);
	private readonly usersService = inject(UsersService);
	private readonly overlay = inject(OverlayService);
	private readonly tabGroup = inject(MatTabGroup);
	private readonly snackbar = inject(MatSnackBar);
	private readonly fb = inject(FormBuilder);
	private readonly tab = inject(MatTab);

	// Input props
	@Input() form!: TD.Types.IForm;
	@Input() quarter!: TD.Types.IQuarter;
	@Input() form_lesson!: TD.Types.IFormLesson | null;

	timetableResponse?: TD.Types.ITimetableResponse;
	diary?: TD.Types.IDiaryEntry[];
	students?: TD.Types.IStudent[];

	formGroup: QuarterDiaryFormGroup = this.fb.nonNullable.group({
		diary: this.fb.array<FormGroup>([]),
		timetable: this.fb.array<FormGroup>([]),
	});

	// State props
	private _pendingChanges = false;

	constructor() {
		super();

		// Implement local change detection
		this.changeRef.detach();
	}

	ngAfterViewInit() {
		// watch for tab changes and update data as needed
		this.tabGroup.selectedTabChange
			.pipe(takeUntil(this.destroy$))
			.subscribe((e) => {
				if (e.tab === this.tab && this._pendingChanges) this.FetchTimetable();
			});

		if (!this._pendingChanges) this.FetchTimetable();
	}

	ngOnChanges() {
		this._pendingChanges = true;
		if (!this.tab.isActive) return;
		this.FetchTimetable();
	}

	SubmitTimetableAndDiary(): void {
		const form_payload = this.formGroup.getRawValue();

		this.usersService.User.pipe(
			mergeMap((u) => {
				const diary_payload: TD.Types.IDiaryPayload = {
					diary: form_payload.diary,
					user_id: u?.id,
				};
				const timetable_payload: Partial<TD.Types.ITimetableEntry>[] =
					form_payload.timetable.map((v) => ({
						id: v.timetable_entry_id,
						homework: v.homework,
						lesson_topic: v.lesson_topic,
					}));

				return forkJoin([
					this.apiClient.Post<boolean>(`/diary`, diary_payload),
					this.apiClient.Patch<boolean>(`/timetable`, timetable_payload),
				]);
			}),
			HandleCRUDRequest(this.snackbar, true),
		).subscribe();
	}

	FetchTimetable(): void {
		// Clear existing data
		this.timetableResponse = undefined;
		this.diary = undefined;
		this.changeRef.detectChanges();

		// Show loader
		this.overlay.createOverlay(LoadingComponent);

		this.apiClient
			.Get<TD.Types.ITimetableResponse, TD.Types.ITimetableRequest>(
				`/timetable`,
				{
					request: {
						request_opts: { with_diary: true },
						filtering_opts: {
							form_id: this.form.id,
							quarter_id: this.quarter.id,
							form_lesson_id: this.form_lesson?.id,
						},
					},
				},
			)
			.subscribe((rt) => {
				this.timetableResponse = rt.data;
				this.diary = this.timetableResponse?.timetable.flatMap(
					(t) => (<any>t).diary,
				);

				this._PatchTimetable();
				this._PatchDiary();

				if (this.form_lesson?.form_subgroup) {
					this.students = this.form_lesson.form_subgroup.students;
				} else {
					this.students = this.form.students;
				}

				// Commit changes and clear loader
				this.overlay.clearOverlay();
				this._pendingChanges = false;
				this.changeRef.detectChanges();
			});
	}

	GetLessonDate(entry: TD.Types.ITimetableEntry): string {
		if (!entry.quarter_week) return "";

		const weekStart = DateTime.fromISO(<any>entry.quarter_week.start_date);
		return (
			weekStart
				.plus({ days: entry.weekday - (weekStart.weekday - 1) })
				.toISODate() ?? ""
		);
	}

	GetMarks(
		entry: TD.Types.ITimetableEntry,
		student: TD.Types.IStudent,
	): (string | undefined)[] {
		return this.formGroup
			.getRawValue()
			.diary.filter(
				(v) => v.student_id === student.id && v.timetable_entry_id === entry.id,
			)
			.flatMap((v) => v.marks);
	}

	private _PatchDiary(): void {
		if (!this.diary) return;

		this.formGroup.controls.diary.clear();
		for (const e of this.diary) {
			this.formGroup.controls.diary.push(
				<any>this.fb.nonNullable.group({
					timetable_entry_id: this.fb.control<number>(e.timetable_entry_id),
					student_id: this.fb.control<number>(e.student_id),
					marks: this.fb.array<FormControl<string>>(
						e.marks?.map((m) => this.fb.nonNullable.control<string>(m)),
					),
					comment: this.fb.control<string | undefined>(e.comment),
				}),
			);
		}
	}

	private _PatchTimetable(): void {
		if (!this.timetableResponse) return;
		if (!this.timetableResponse.timetable) return;

		this.formGroup.controls.timetable.clear();
		for (const t of this.timetableResponse.timetable) {
			this.formGroup.controls.timetable.push(
				this.fb.group({
					timetable_entry_id: this.fb.nonNullable.control<number>(t.id),
					homework: this.fb.nonNullable.control<string | undefined>(t.homework),
					lesson_topic: this.fb.nonNullable.control<string | undefined>(
						t.lesson_topic,
					),
				}),
			);
		}
	}

	protected readonly TD = TD;
}

export type QuarterDiaryFormGroup = FormGroup<{
	diary: FormArray<DiaryPayloadFormGroup>;
	timetable: FormArray<TimetablePayloadFormGroup>;
}>;

export type DiaryPayloadFormGroup = FormGroup<{
	timetable_entry_id: FormControl<number>;
	student_id: FormControl<number>;
	marks: FormArray<FormControl<string>>;
	comment: FormControl<string | undefined>;
}>;

export type TimetablePayloadFormGroup = FormGroup<{
	timetable_entry_id: FormControl<number>;
	lesson_topic: FormControl<string | undefined>;
	homework: FormControl<string | undefined>;
}>;
