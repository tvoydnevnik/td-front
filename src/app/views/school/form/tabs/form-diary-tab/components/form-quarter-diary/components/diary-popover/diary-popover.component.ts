import { ControlContainer, FormBuilder, FormControl } from "@angular/forms";
import { QuarterDiaryFormGroup } from "../../form-quarter-diary.component";
import { MARKS } from "@td/constants/diary-constants";
import { SharedModule } from "@shared/shared.module";
import { TippyDirective } from "@ngneat/helipopper";
import { CommonModule } from "@angular/common";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	EventEmitter,
	inject,
	Input,
	OnDestroy,
	Output,
} from "@angular/core";
import { TD } from "@td";

@Component({
	selector: "app-diary-popover",
	standalone: true,
	imports: [CommonModule, SharedModule],
	templateUrl: "./diary-popover.component.html",
	styleUrls: ["./diary-popover.component.scss"],
})
export class DiaryPopoverComponent implements AfterViewInit, OnDestroy {
	// Injected props
	private readonly _controlContainer = inject(ControlContainer);
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly fb = inject(FormBuilder);

	// Input props
	@Input() timetable_entry?: TD.Types.ITimetableEntry;
	@Input() student?: TD.Types.IStudent;
	@Input() tippy?: TippyDirective;

	// Event outputs
	@Output() changes = new EventEmitter<boolean>();

	private externalFormGroup?: QuarterDiaryFormGroup;
	marksArray = this.fb.array<FormControl<string>>([]);
	comment: FormControl<string | undefined> | undefined =
		this.fb.nonNullable.control<string>("");

	ngAfterViewInit() {
		if (!this.timetable_entry) throw Error("Timetable entry not provided!");
		if (!this.student) throw Error("Student not provided!");

		this._SetupFormControl();
	}

	ngOnDestroy() {
		this._CleanupForm();
	}

	TriggerChanges(propagate = true): void {
		this.changeRef.detectChanges();

		if (propagate) this.changes.emit(true);
	}

	AddMarkField(): void {
		this.marksArray?.controls.push(this.fb.nonNullable.control<string>(""));
		this.TriggerChanges();
	}

	protected Hide(): void {
		this.tippy?.hide();
	}

	private _SetupFormControl(): void {
		if (!this._controlContainer) return;
		if (!this._controlContainer.control) return;

		this.externalFormGroup = <QuarterDiaryFormGroup>(
			this._controlContainer.control
		);

		let diaryEntryControl = this.externalFormGroup.controls.diary.controls.find(
			({ value: v }) =>
				v.student_id === this.student?.id &&
				v.timetable_entry_id === this.timetable_entry?.id,
		);

		if (diaryEntryControl) {
			this.comment = diaryEntryControl.controls.comment;
		}

		if (!diaryEntryControl) {
			this.externalFormGroup.controls.diary.push(
				this.fb.group({
					timetable_entry_id: this.fb.nonNullable.control(
						this.timetable_entry!.id,
					),
					student_id: this.fb.nonNullable.control(this.student!.id),
					marks: this.fb.array<FormControl<string>>([]),
					comment: this.comment,
				}),
			);

			diaryEntryControl = this.externalFormGroup.controls.diary.at(-1);
		}

		if (diaryEntryControl?.controls.marks?.controls.length === 0) {
			diaryEntryControl.setControl("marks", this.marksArray);
		} else {
			this.marksArray = diaryEntryControl.controls.marks;
		}

		this.changeRef.detectChanges();
	}

	private _CleanupForm(): void {
		const to_remove: number[] = [];
		this.marksArray?.controls.forEach((v, i) => {
			if (v.getRawValue() === null) to_remove.push(i);
		});

		to_remove.forEach((v, i) => {
			this.marksArray.removeAt(v - i);
		});
	}

	protected readonly TD = TD;
	protected readonly MARKS = MARKS;
}
