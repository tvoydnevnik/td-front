import { ChangeDetectorRef, Component, inject, Input } from "@angular/core";
import { QuarterlyMarkForm } from "../../form-year-diary.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MARKS } from "@td/constants/diary-constants";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { TippyDirective } from "@ngneat/helipopper";
import { CommonModule } from "@angular/common";
import { TD } from "@td";

@Component({
	selector: "app-quarterly-mark-popover",
	standalone: true,
	imports: [
		CommonModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		ReactiveFormsModule,
		SharedModule,
	],
	templateUrl: "./quarterly-mark-popover.component.html",
	styleUrls: ["./quarterly-mark-popover.component.scss"],
})
export class QuarterlyMarkPopoverComponent {
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);

	// Input props
	@Input() formGroup?: QuarterlyMarkForm;
	@Input() quarter?: TD.Types.IQuarter;
	@Input() student?: TD.Types.IStudent;
	@Input() tippy?: TippyDirective;

	TriggerChanges(): void {
		this.changeRef.detectChanges();
	}

	Hide(): void {
		this.tippy?.hide();
	}

	protected readonly TD = TD;
	protected readonly MARKS = MARKS;
}
