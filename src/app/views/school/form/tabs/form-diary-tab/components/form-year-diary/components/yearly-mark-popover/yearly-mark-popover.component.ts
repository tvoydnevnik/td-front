import { ChangeDetectorRef, Component, inject, Input } from "@angular/core";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { YearlyMarkForm } from "../../form-year-diary.component";
import { MatButtonModule } from "@angular/material/button";
import { MARKS } from "@td/constants/diary-constants";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { TippyDirective } from "@ngneat/helipopper";
import { CommonModule } from "@angular/common";
import { TD } from "@td";

@Component({
	selector: "app-yearly-mark-popover",
	standalone: true,
	imports: [
		CommonModule,
		MatButtonModule,
		MatButtonToggleModule,
		ReactiveFormsModule,
		SharedModule,
	],
	templateUrl: "./yearly-mark-popover.component.html",
	styleUrls: ["./yearly-mark-popover.component.scss"],
})
export class YearlyMarkPopoverComponent {
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);

	// Input props
	@Input() formGroup?: YearlyMarkForm;
	@Input() student?: TD.Types.IStudent;
	@Input() tippy?: TippyDirective;

	TriggerChanges(): void {
		this.changeRef.detectChanges();
	}

	Hide(): void {
		this.tippy?.hide();
	}

	protected readonly TD = TD;
	protected readonly MARKS = MARKS;
}
