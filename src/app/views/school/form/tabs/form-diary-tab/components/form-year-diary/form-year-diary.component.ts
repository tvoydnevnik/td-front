import { QuarterlyMarkPopoverComponent } from "./components/quarterly-mark-popover/quarterly-mark-popover.component";
import { FormDiaryPopoverToggleComponent } from "../form-diary-popover-toggle/form-diary-popover-toggle.component";
import { YearlyMarkPopoverComponent } from "./components/yearly-mark-popover/yearly-mark-popover.component";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ApiClientService } from "@td/api/api-client.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UsersService } from "@services/users.service";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import { mergeMap } from "rxjs";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
} from "@angular/core";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		FormDiaryPopoverToggleComponent,
		QuarterlyMarkPopoverComponent,
		YearlyMarkPopoverComponent,
		SharedModule,
	],
	selector: "app-form-year-diary",
	templateUrl: "./form-year-diary.component.html",
	styleUrls: ["./form-year-diary.component.scss"],
})
export class FormYearDiaryComponent implements AfterViewInit {
	// Input props
	@Input() quarters?: TD.Types.IQuarter[];
	@Input() form?: TD.Types.IForm;
	@Input() form_lesson?: TD.Types.IFormLesson;

	// Injected props
	public readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);
	private readonly usersService = inject(UsersService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly fb = inject(FormBuilder);

	schoolYear?: TD.Types.ISchoolYear;
	quarterlyMarks: QuarterlyMarkForm[] = [];
	yearlyMarks: YearlyMarkForm[] = [];
	students?: TD.Types.IStudent[];

	constructor() {
		this.changeRef.detach();
	}

	ngAfterViewInit() {
		this.apiClient
			.Get<TD.Types.IFinalMarksResponse, TD.Types.IFinalMarksRequest>(
				`/final-marks`,
				{
					request: {
						request_opts: { for_year: true },
						filtering_opts: {
							form_id: this.form?.id,
							form_lesson_id: this.form_lesson?.id,
						},
					},
				},
			)
			.subscribe((r) => {
				this.schoolYear = r.data?.school_year;
				this.quarterlyMarks =
					r.data?.quarterly?.map((v) =>
						this.fb.nonNullable.group({
							form_lesson_id: v.form_lesson_id,
							quarter_id: v.quarter_id,
							student_id: v.student_id,
							mark: v.mark,
						}),
					) ?? [];
				this.yearlyMarks =
					r.data?.yearly?.map((v) =>
						this.fb.nonNullable.group({
							form_lesson_id: v.form_lesson_id,
							school_year_id: v.school_year_id,
							student_id: v.student_id,
							mark: v.mark,
						}),
					) ?? [];

				if (this.form_lesson?.form_subgroup) {
					this.students = this.form_lesson.form_subgroup.students;
				} else {
					this.students = this.form?.students;
				}

				this.changeRef.detectChanges();
			});
	}

	GetQuarterMarkForm(
		quarter?: TD.Types.IQuarter,
		student?: TD.Types.IStudent,
	): QuarterlyMarkForm | undefined {
		let control = this.quarterlyMarks.find(
			(c) =>
				c.value["student_id"] === student?.id &&
				c.value["quarter_id"] === quarter?.id,
		);

		if (!control) {
			this.quarterlyMarks.push(
				this.fb.nonNullable.group({
					quarter_id: quarter?.id ?? -1,
					student_id: student?.id ?? -1,
					form_lesson_id: this.form_lesson?.id ?? -1,
					mark: this.fb.nonNullable.control<string | undefined>(""),
				}),
			);
			control = this.quarterlyMarks.at(-1);
		}

		return control;
	}

	GetYearMarkForm(
		year?: TD.Types.ISchoolYear,
		student?: TD.Types.IStudent,
	): YearlyMarkForm | undefined {
		let control = this.yearlyMarks.find(
			(c) =>
				c.value["student_id"] === student?.id &&
				c.value["school_year_id"] === year?.id,
		);

		if (!control) {
			this.yearlyMarks.push(
				this.fb.nonNullable.group({
					school_year_id: year?.id ?? -1,
					student_id: student?.id ?? -1,
					form_lesson_id: this.form_lesson?.id ?? -1,
					mark: this.fb.nonNullable.control<string | undefined>(""),
				}),
			);
			control = this.yearlyMarks.at(-1);
		}

		return control;
	}

	GetQuarterMark(
		quarter?: TD.Types.IQuarter,
		student?: TD.Types.IStudent,
	): string | undefined {
		if (!quarter) return;
		if (!student) return;

		return this.quarterlyMarks?.find(
			(v) =>
				v.value.quarter_id === quarter.id && v.value.student_id === student.id,
		)?.value.mark;
	}

	GetYearMark(student?: TD.Types.IStudent): string | undefined {
		if (!student) return;

		return this.yearlyMarks?.find((v) => v.value.student_id === student.id)
			?.value.mark;
	}

	GetAvgMark(student?: TD.Types.IStudent): string {
		const marks = this.quarterlyMarks
			?.filter((v) => v.value.student_id === student?.id)
			.map((v) => parseInt(v.value.mark ?? ""));
		if (!marks || marks.length === 0) return "-";
		if (marks.every((v) => isNaN(v))) return "-";

		let diff = 0;
		const sum = marks.reduce((acc, curr) => {
			if (isNaN(curr)) {
				diff++;
				return acc;
			}

			return acc + curr;
		}, 0);
		const len = marks.length - diff;

		return len > 0 ? (sum / len).toFixed(2).toString() : "-";
	}

	ValidateMarks(): boolean {
		return (
			this.quarterlyMarks.every((v) => v.valid) &&
			this.yearlyMarks.every((v) => v.valid)
		);
	}

	PostFinalMarks(): void {
		const payload = {
			quarterly_marks: this.quarterlyMarks.map((v) => v.getRawValue()),
			yearly_marks: this.yearlyMarks.map((v) => v.getRawValue()),
		};

		this.usersService.User.pipe(
			mergeMap((u) =>
				this.apiClient.Post<boolean, TD.Types.IFinalMarksPayload>(
					`/final-marks`,
					{ ...payload, user_id: u?.id },
				),
			),
			TD.HandleCRUDRequest(this.snackbar, true),
		).subscribe();
	}
}

export type QuarterlyMarkForm = FormGroup<{
	form_lesson_id: FormControl<number>;
	student_id: FormControl<number>;
	mark: FormControl<string | undefined>;
	quarter_id: FormControl<number>;
}>;

export type YearlyMarkForm = FormGroup<{
	form_lesson_id: FormControl<number>;
	student_id: FormControl<number>;
	mark: FormControl<string | undefined>;
	school_year_id: FormControl<number>;
}>;
