import { TippyDirective } from "@ngneat/helipopper";
import { CommonModule } from "@angular/common";
import { Content } from "@ngneat/overview";
import {
	Component,
	EventEmitter,
	Input,
	Output,
	TemplateRef,
} from "@angular/core";

@Component({
	selector: "app-form-diary-popover-toggle",
	standalone: true,
	imports: [CommonModule, TippyDirective],
	templateUrl: "./form-diary-popover-toggle.component.html",
	styleUrls: ["./form-diary-popover-toggle.component.scss"],
})
export class FormDiaryPopoverToggleComponent {
	@Input() popover_template?: Content;
	@Input() popover_data: any = {};
	@Input() label?: string;

	@Input() toggle_content: TemplateRef<any> | null = null;
	@Input() toggle_content_context: any;

	@Output() visibilityChanged = new EventEmitter<{ visible: boolean }>();

	protected VisibilityChanged(visible: boolean): void {
		this.visibilityChanged.emit({ visible });
	}
}
