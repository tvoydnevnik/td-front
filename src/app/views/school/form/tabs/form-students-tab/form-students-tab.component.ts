import { CommonModule, NgOptimizedImage } from "@angular/common";
import { SharedModule } from "@shared/shared.module";
import { Component, Input } from "@angular/core";
import { RouterLink } from "@angular/router";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule, RouterLink, NgOptimizedImage],
	selector: "app-form-students-tab",
	templateUrl: "./form-students-tab.component.html",
	styleUrls: ["./form-students-tab.component.scss"],
})
export class FormStudentsTabComponent {
	@Input() form!: TD.Types.IForm;
}
