import { ApiClientService } from "@td/api/api-client.service";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
} from "@angular/core";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-form-timetable-tab",
	templateUrl: "./form-timetable-tab.component.html",
	styleUrls: ["./form-timetable-tab.component.scss"],
})
export class FormTimetableTabComponent implements AfterViewInit {
	@Input() form!: TD.Types.IForm;
	timetable?: TD.Types.ITimetableResponse;

	private readonly apiClient = inject(ApiClientService);

	constructor(private readonly changeRef: ChangeDetectorRef) {}

	ngAfterViewInit() {
		this.apiClient
			.Get<TD.Types.ITimetableResponse, TD.Types.ITimetableRequest>(
				`/timetable`,
				{
					request: {
						request_opts: { for_week: true },
						filtering_opts: { form_id: this.form.id },
					},
				},
			)
			.subscribe((r) => {
				this.timetable = r.data!;
				this.changeRef.detectChanges();
			});
	}
}
