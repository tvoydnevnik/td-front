import { ApiClientService } from "@td/api/api-client.service";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
} from "@angular/core";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-student-timetable-tab",
	templateUrl: "./student-timetable-tab.component.html",
	styleUrls: ["./student-timetable-tab.component.scss"],
})
export class StudentTimetableTabComponent implements AfterViewInit {
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);

	@Input() student!: TD.Types.IStudent;
	timetable?: TD.Types.ITimetableResponse;

	constructor() {}

	ngAfterViewInit() {
		this.apiClient
			.Get<TD.Types.ITimetableResponse, TD.Types.ITimetableRequest>(
				`/timetable`,
				{
					request: {
						request_opts: { for_week: true },
						filtering_opts: { student_id: this.student.id },
					},
				},
			)
			.subscribe((r) => {
				this.timetable = r.data;
				this.changeRef.detectChanges();
			});
	}
}
