import { DiaryComponent } from "../../../../diary/diary.component";
import { SharedModule } from "@shared/shared.module";
import { Component, Input } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TD } from "@td";

@Component({
	selector: "app-student-diary-tab",
	standalone: true,
	imports: [CommonModule, SharedModule, DiaryComponent],
	templateUrl: "./student-diary-tab.component.html",
	styleUrls: ["./student-diary-tab.component.scss"],
})
export class StudentDiaryTabComponent {
	// Input props
	@Input() student?: TD.Types.IStudent;
}
