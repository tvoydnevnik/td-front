import { StudentTimetableTabComponent } from "./tabs/student-timetable-tab/student-timetable-tab.component";
import { StudentDiaryTabComponent } from "./tabs/student-diary-tab/student-diary-tab.component";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { ApiClientService } from "@td/api/api-client.service";
import { ActivatedRoute, RouterLink } from "@angular/router";
import { SharedModule } from "@shared/shared.module";
import { AuthService } from "@services/auth.service";
import { Component, inject } from "@angular/core";
import { map, mergeMap, Observable } from "rxjs";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		SharedModule,
		RouterLink,
		StudentTimetableTabComponent,
		NgOptimizedImage,
		StudentDiaryTabComponent,
	],
	selector: "app-student",
	templateUrl: "./student.component.html",
	styleUrls: ["./student.component.scss"],
})
export class StudentComponent {
	// Injected props
	private readonly apiClient = inject(ApiClientService);
	private readonly route = inject(ActivatedRoute);
	public readonly authService = inject(AuthService);

	student?: TD.Types.IStudent;
	private id?: string | null;

	constructor() {
		this.route.paramMap
			.pipe(
				mergeMap((map) => {
					this.id = map.get("id");
					return this.apiClient.Get(`/students/${this.id}`);
				}),
			)
			.subscribe((r) => (this.student = r.data));
	}

	CheckDiaryAccess(): Observable<boolean> {
		return this.authService.ValidateRoles(TD.Types.UserRole.TEACHER).pipe(
			map((r) => {
				const { has_access, user } = r;

				if (!user) return false;
				if (user.id === this.student?.user_id) return true;
				if (has_access) return true;
				return (
					this.student?.parents?.some((v) => v.user_id === user.id) ?? false
				);
			}),
		);
	}

	protected readonly TD = TD;
}
