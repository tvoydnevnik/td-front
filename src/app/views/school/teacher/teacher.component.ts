import { ApiClientService } from "@td/api/api-client.service";
import { Component, inject } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-teacher",
	templateUrl: "./teacher.component.html",
	styleUrls: ["./teacher.component.scss"],
})
export class TeacherComponent {
	// Injected props
	private readonly apiClient = inject(ApiClientService);
	private readonly route = inject(ActivatedRoute);

	teacher?: TD.Types.ITeacher;
	timetable?: TD.Types.ITimetableResponse;

	constructor() {
		this.route.paramMap
			.pipe(mergeMap((m) => this.apiClient.Get(`/teachers/${m.get("id")}`)))
			.subscribe((r) => (this.teacher = r.data));
	}
}
