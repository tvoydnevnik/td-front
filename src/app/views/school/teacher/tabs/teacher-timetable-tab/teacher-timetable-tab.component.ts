import { ApiClientService } from "@td/api/api-client.service";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
} from "@angular/core";
import { TD } from "@td";

@Component({
	selector: "app-teacher-timetable-tab",
	templateUrl: "./teacher-timetable-tab.component.html",
	styleUrls: ["./teacher-timetable-tab.component.scss"],
})
export class TeacherTimetableTabComponent implements AfterViewInit {
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);

	@Input() teacher!: TD.Types.ITeacher;
	timetable?: TD.Types.ITimetableResponse;

	constructor() {}

	ngAfterViewInit() {
		this.apiClient
			.Get<TD.Types.ITimetableResponse, TD.Types.ITimetableRequest>(
				`/timetable`,
				{
					request: {
						request_opts: { for_week: true },
						filtering_opts: { teacher_id: this.teacher.id },
					},
				},
			)
			.subscribe((r) => {
				this.timetable = r.data;
				this.changeRef.detectChanges();
			});
	}
}
