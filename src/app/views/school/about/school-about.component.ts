import { SchoolManagementService } from "@services/school-management.service";
import { ChangeDetectorRef, Component, inject } from "@angular/core";
import { TD } from "@td";

@Component({
	selector: "app-about",
	templateUrl: "./school-about.component.html",
	styleUrls: ["./school-about.component.scss"],
})
export class SchoolAboutComponent {
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);

	info?: TD.Types.ISchool;

	constructor(private readonly schoolService: SchoolManagementService) {
		this.schoolService.GetInfo().subscribe((resp) => {
			this.info = resp.data;
			this.changeRef.detectChanges();
		});
	}
}
