import { TeacherTimetableTabComponent } from "./teacher/tabs/teacher-timetable-tab/teacher-timetable-tab.component";
import { SchoolAboutComponent } from "./about/school-about.component";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { TeacherComponent } from "./teacher/teacher.component";
import { SchoolRoutingModule } from "./school.routing.module";
import { FormsComponent } from "./forms/forms.component";
import { SharedModule } from "@shared/shared.module";
import { NgModule } from "@angular/core";

const COMPONENTS = [
	TeacherTimetableTabComponent,
	SchoolAboutComponent,
	TeacherComponent,
	FormsComponent,
];

@NgModule({
	imports: [SchoolRoutingModule, CommonModule, SharedModule, NgOptimizedImage],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
})
export class SchoolModule {}
