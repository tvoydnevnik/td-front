import { ApiClientService } from "@td/api/api-client.service";
import { map, mergeMap, Subject, takeUntil } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { SharedModule } from "@shared/shared.module";
import { ActivatedRoute } from "@angular/router";
import { CommonModule } from "@angular/common";
import {
	HandleCRUDRequest,
	HandleHTTPError,
} from "@td/utils/crud-operations-helpers";
import {
	ConstructTimetableFillingData,
	ConstructTimetableFormGroup,
	LessonDataForm,
} from "./models/timetable.models";
import { Info } from "luxon";
import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	inject,
	OnDestroy,
} from "@angular/core";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-timetable-wizard",
	templateUrl: "./timetable-wizard.component.html",
	styleUrls: ["./timetable-wizard.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class TimetableWizardComponent
	implements AfterViewInit, OnDestroy
{
	// Injected props
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);
	private readonly router = inject(RouterService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly fb = inject(FormBuilder);

	baseForm = this.fb.group({
		form: this.fb.nonNullable.control<TD.Types.IForm | undefined>(
			undefined,
			Validators.required,
		),
		quarter: this.fb.nonNullable.control<TD.Types.IQuarter | undefined>(
			undefined,
			Validators.required,
		),
		bells_schedule: this.fb.nonNullable.control<
			TD.Types.IBellsSchedule | undefined
		>(undefined, Validators.required),
		lessons: this.fb.nonNullable.array([]),
	});

	// base form configs
	quarterSelectConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Quarter",
		options: this.apiClient
			.Get<TD.Types.IQuarter[]>("/quarters")
			.pipe(map((r) => r.data!)),
		displayWith: TD.QuarterDisplay,
		required: true,
		formControlName: "quarter",
	});
	bellsScheduleSelectConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Schedule",
		options: this.apiClient
			.Get<TD.Types.IBellsSchedule[]>("/school/bell-schedules")
			.pipe(map((r) => r.data!)),
		displayWith: TD.BellsScheduleGroupDisplay,
		required: true,
		formControlName: "bells_schedule",
	});

	// timetable configs
	lessonConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Lesson",
		options: this.apiClient
			.Get<TD.Types.ILesson[]>("/lessons")
			.pipe(map((r) => r.data ?? [])),
		displayWith: TD.LessonDisplay,
		formControlName: "lesson",
		required: true,
	});
	teacherConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Teacher",
		options: this.apiClient
			.Get<TD.Types.ITeacher[]>("/teachers")
			.pipe(map((r) => r.data ?? [])),
		displayWith: TD.TeacherDisplay,
		formControlName: "teacher",
		required: true,
	});
	subgroupConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Subgroup",
		options: [],
		displayWith: TD.FormSubgroupDisplay,
		formControlName: "form_subgroup",
		required: false,
	});

	weekdays = Info.weekdays("long");
	lessonForms = new Map<number, LessonDataForm[]>(
		this.weekdays.map((_, i) => [i, []]),
	);

	private _$destroy = new Subject<void>();

	ngAfterViewInit() {
		this._FetchConfigData();
	}

	constructor() {}

	ngOnDestroy() {
		this._$destroy.next();
		this._$destroy.complete();
	}

	GetLessonTitle(data: ReturnType<LessonDataForm["getRawValue"]>): string {
		return `${data.index}. ${data.lesson ? data.lesson.name : "Lesson"} by ${
			data.teacher ? TD.UserFullNameDisplay(data.teacher.user) : "teacher"
		} in ${data.classroom ? data.classroom : "unit"}${
			data.form_subgroup ? ` (${data.form_subgroup.name})` : ""
		}`;
	}

	AddSubgroup(weekday: number): void {
		const lastLesson = this._GetWeekdayLesson(weekday, -1);
		const l = this.AddLesson(weekday);

		if (lastLesson)
			l.patchValue({
				index: lastLesson.getRawValue().index,
			});
	}

	AddLesson(weekday: number): LessonDataForm {
		const formGroup = ConstructTimetableFormGroup();
		const lastLesson = this._GetWeekdayLesson(weekday, -1);
		formGroup.patchValue({
			index: lastLesson ? lastLesson.getRawValue().index + 1 : 1,
		});

		this.GetWeekdayLessons(weekday).push(formGroup);
		this.changeRef.detectChanges();

		return formGroup;
	}

	RemoveLesson(weekday: number, lesson: number): void {
		this.GetWeekdayLessons(weekday).splice(lesson, 1);
	}

	SubmitTimetable(): void {
		const baseFormData = this.baseForm.getRawValue();
		const payload: TD.Types.ITimetablePayload = {
			form: baseFormData.form!.id,
			quarter: baseFormData.quarter!.id,
			payload: ConstructTimetableFillingData(
				this.lessonForms,
				baseFormData.bells_schedule!,
			),
		};

		this.apiClient
			.Post<TD.Types.ITimetableEntry[]>(`/timetable/fill`, payload)
			.pipe(HandleCRUDRequest(this.snackbar))
			.subscribe(() => this.router.NavigateBack());
	}

	GetWeekdayLessons(weekday: number): LessonDataForm[] {
		return this.lessonForms
			.get(weekday)!
			.sort((a, b) => a.getRawValue().index - b.getRawValue().index);
	}

	protected _ValidateTimetable(): boolean {
		let hasElements = false;
		for (let day of this.lessonForms.values()) {
			if (day.length !== 0) hasElements = true;
			const everyFormValid = day.every((v) => v.valid);
			if (!everyFormValid) return false;
		}

		return hasElements;
	}

	private _GetWeekdayLesson(
		weekday: number,
		lesson: number,
	): LessonDataForm | undefined {
		return this.GetWeekdayLessons(weekday).at(lesson);
	}

	private _FetchConfigData(): void {
		// Get form_id and fetch form info.
		// Indicate error if id format is bad or form was not found.
		this.activatedRoute.paramMap
			.pipe(
				takeUntil(this._$destroy),
				mergeMap((m) => {
					const id = m.get("form_id");
					if (!id)
						return this.snackbar
							.open("Invalid form id", "Back")
							.afterDismissed()
							.pipe(mergeMap(() => this.router.NavigateBack()));

					return this.apiClient.Get<TD.Types.IForm>(`/forms/${id}`);
				}),
				HandleHTTPError(this.snackbar),
			)
			.subscribe((r) => {
				if (typeof r === "boolean") return;

				this.baseForm.patchValue({
					form: r.data,
				});
				this.subgroupConfig.SetOptions(r.data?.subgroups ?? []);
			});
	}
}
