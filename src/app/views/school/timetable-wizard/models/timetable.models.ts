import { FormControl, FormGroup, Validators } from "@angular/forms";
import { DateTime } from "luxon";
import { TD } from "@td";

export const ConstructTimetableFormGroup = (): LessonDataForm =>
	new FormGroup({
		index: new FormControl<number>(0, {
			nonNullable: true,
			validators: [Validators.required, Validators.min(1)],
		}),
		classroom: new FormControl<string>("", {
			nonNullable: true,
			validators: Validators.required,
		}),
		lesson: new FormControl<TD.Types.ILesson | undefined>(undefined, {
			nonNullable: true,
			validators: Validators.required,
		}),
		teacher: new FormControl<TD.Types.ITeacher | undefined>(undefined, {
			nonNullable: true,
			validators: Validators.required,
		}),
		form_subgroup: new FormControl<TD.Types.IFormSubgroup | undefined>(
			undefined,
			{
				nonNullable: true,
			},
		),
	});

export const ConstructTimetableFillingData = (
	data: Map<number, LessonDataForm[]>,
	bells_schedule: TD.Types.IBellsSchedule,
): TD.Types.ITimetablePayloadEntry[] =>
	Array.from(data)
		.map(([weekday, lessons]): TD.Types.ITimetablePayloadEntry[] =>
			lessons.map((formGroup): TD.Types.ITimetablePayloadEntry => {
				const formData = formGroup.getRawValue();
				const schedule = bells_schedule.schedule.find(
					(s: TD.Types.ISchedule) => s.lesson_index === formData.index,
				);

				return {
					index: formData.index,
					classroom: formData.classroom,
					weekday: weekday,
					start_time: schedule
						? DateTime.fromISO(<string>schedule.start_time).toISOTime() ?? ""
						: "",
					end_time: schedule
						? DateTime.fromISO(<string>schedule.end_time).toISOTime() ?? ""
						: "",
					lesson: formData.lesson!.id,
					teacher: formData.teacher!.id,
					form_subgroup: formData.form_subgroup?.id,
				};
			}),
		)
		.flat();

export type LessonDataForm = FormGroup<{
	index: FormControl<number>;
	classroom: FormControl<string>;
	teacher: FormControl<TD.Types.ITeacher | undefined>;
	lesson: FormControl<TD.Types.ILesson | undefined>;
	form_subgroup: FormControl<TD.Types.IFormSubgroup | undefined>;
}>;
