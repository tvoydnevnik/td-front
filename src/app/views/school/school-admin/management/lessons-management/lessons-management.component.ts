import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-lessons-management",
	templateUrl: "./lessons-management.component.html",
	styleUrls: ["./lessons-management.component.scss"],
})
export class LessonsManagementComponent {
	lessons;
	lesson_columns = new TD.ItemProps([
		{
			title: "ID",
			key: "id",
		},
		{
			title: "Name",
			key: "name",
		},
	]);

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.lessons = new TD.ItemsListDatasource<TD.Types.ILesson>(this.changeRef);
		this.FetchLessons();
	}

	FetchLessons(): void {
		this.schoolService
			.GetLessons()
			.subscribe((response) => this.lessons.UpdateData(response.data));
	}

	DeleteLesson(lesson: number): void {
		this.dialogService
			.Open("Are you sure you want ot delete this lesson?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteLesson(lesson)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchLessons());
	}
}
