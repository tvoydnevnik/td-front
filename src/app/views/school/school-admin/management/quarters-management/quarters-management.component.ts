import { ChangeDetectorRef, Component, inject, LOCALE_ID } from "@angular/core";
import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { FormControl } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-quarters-management",
	templateUrl: "./quarters-management.component.html",
	styleUrls: ["./quarters-management.component.scss"],
})
export class QuartersManagementComponent {
	private readonly locale = inject(LOCALE_ID);
	private readonly _DateTransformations = [
		{ pipe: new DatePipe(this.locale), arguments: ["fullDate"] },
	];

	quartersStore?: TD.Types.IQuarter[];
	quarters;
	quarter_columns = new TD.ItemProps([
		{
			title: "ID",
			key: "id",
		},
		{
			title: "Starts",
			key: "start_date",
			transform: this._DateTransformations,
		},
		{
			title: "Ends",
			key: "end_date",
			transform: this._DateTransformations,
		},
		{
			title: "Default from",
			key: "indexing_start",
			transform: this._DateTransformations,
		},
		{
			title: "Default to",
			key: "indexing_end",
			transform: this._DateTransformations,
		},
		{
			title: "School Year",
			location: [{ key: "school_year" }, { key: "id" }],
		},
	]);

	years?: TD.Types.ISchoolYear[];
	year_selector = new FormControl<number | undefined>(undefined, {
		nonNullable: true,
	});
	currentYear?: number = undefined;

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.quarters = new TD.ItemsListDatasource<TD.Types.IQuarter>(
			this.changeRef,
		);
		this.FetchYears();
		this.FetchQuarters();
	}

	FetchQuarters(): void {
		this.schoolService.GetQuarters().subscribe({
			next: (response) => {
				this.quartersStore = response.data;
				this.OnYearSelected({ value: this.currentYear });
			},
		});
	}

	FetchYears(): void {
		this.schoolService.GetSchoolYears().subscribe({
			next: (response) => (this.years = response.data),
		});
	}

	DeleteQuarter(id: number): void {
		this.dialogService
			.Open("Are you sure you want to delete this quarter?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteQuarter(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchQuarters());
	}

	GenerateWeeks(id: number): void {
		this.dialogService
			.Open(
				"Are you sure you want to generate weeks for this quarter?",
				"This action is irreversible and you will not be able to use this feature for the same quarter again.",
			)
			.pipe(
				mergeMap(() => this.schoolService.GenerateQuarterWeeks(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchQuarters());
	}

	OnYearSelected(evt: { value?: number }): void {
		if (!evt.value) {
			this.quarters.UpdateData(this.quartersStore);
			return;
		}

		this.currentYear = evt.value;
		const quarters = this.quartersStore?.filter(
			(quarter) => quarter.school_year?.id === evt.value,
		);
		this.quarters.UpdateData(quarters);
	}
}
