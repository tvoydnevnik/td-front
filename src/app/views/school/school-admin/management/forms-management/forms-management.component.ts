import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-forms-management",
	templateUrl: "./forms-management.component.html",
	styleUrls: ["./forms-management.component.scss"],
})
export class FormsManagementComponent {
	forms;
	form_columns = new TD.ItemProps([
		{
			title: "ID",
			key: "id",
		},
		{
			title: "Grade",
			key: "grade",
		},
		{
			title: "Letter",
			key: "letter",
		},
		{
			title: "Students",
			location: [{ key: "students" }, { key: "length" }],
		},
		{
			title: "Graduated",
			key: "graduated",
		},
	]);

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.forms = new TD.ItemsListDatasource<TD.Types.IForm>(this.changeRef);
		this.FetchForms();
	}

	FetchForms(): void {
		this.schoolService.GetForms().subscribe({
			next: (response) => this.forms.UpdateData(response.data),
		});
	}

	DeleteForm(id: number): void {
		this.dialogService
			.Open("Are you sure you want ot delete this form?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteFrom(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchForms());
	}
}
