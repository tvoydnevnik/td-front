import { BellsScheduleManagementComponent } from "./bells-schedule-management/bells-schedule-management.component";
import { FormSubgroupsManagementComponent } from "./form-subgroups-management/form-subgroups-management.component";
import { QuarterWeeksManagementComponent } from "./quarter-weeks-management/quarter-weeks-management.component";
import { SchoolAboutManagementComponent } from "./school-about-management/school-about-management.component";
import { SchoolYearsManagementComponent } from "./school-years-management/school-years-management.component";
import { QuartersManagementComponent } from "./quarters-management/quarters-management.component";
import { UserbaseManagementComponent } from "./userbase-management/userbase-management.component";
import { LessonsManagementComponent } from "./lessons-management/lessons-management.component";
import { FormsManagementComponent } from "./forms-management/forms-management.component";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

const routes: Routes = [
	{
		path: "about",
		component: SchoolAboutManagementComponent,
	},
	{
		path: "users",
		component: UserbaseManagementComponent,
	},
	{
		path: "bells-schedule",
		component: BellsScheduleManagementComponent,
	},
	{
		path: "forms",
		component: FormsManagementComponent,
	},
	{
		path: "form-subgroups",
		component: FormSubgroupsManagementComponent,
	},
	{
		path: "lessons",
		component: LessonsManagementComponent,
	},
	{
		path: "school-years",
		component: SchoolYearsManagementComponent,
	},
	{
		path: "quarters",
		component: QuartersManagementComponent,
	},
	{
		path: "quarter-weeks",
		component: QuarterWeeksManagementComponent,
	},
	{
		path: "",
		pathMatch: "full",
		redirectTo: "about",
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SchoolManagementRoutingModule {}
