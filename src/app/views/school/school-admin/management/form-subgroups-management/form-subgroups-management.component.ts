import { SchoolManagementService } from "@services/school-management.service";
import { UserDisplayNamePipe } from "@shared/pipes/user-display-name.pipe";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-form-subgroups-management",
	templateUrl: "./form-subgroups-management.component.html",
	styleUrls: ["./form-subgroups-management.component.scss"],
})
export class FormSubgroupsManagementComponent {
	subgroups;
	subgroup_columns = new TD.ItemProps([
		{
			title: "ID",
			key: "id",
		},
		{
			title: "Name",
			key: "name",
		},
		{
			title: "Students",
			location: [{ key: "students", isArray: true }, { key: "user" }],
			transform: [{ pipe: UserDisplayNamePipe }],
		},
	]);

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.subgroups = new TD.ItemsListDatasource<TD.Types.IFormSubgroup>(
			this.changeRef,
		);
		this.FetchSubgroups();
	}

	FetchSubgroups(): void {
		this.schoolService
			.GetSubgroups()
			.subscribe((resp) => this.subgroups.UpdateData(resp.data));
	}

	DeleteSubgroup(subgroup: number): void {
		this.dialogService
			.Open("Are you sure you want ot delete this form subgroup?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteSubgroup(subgroup)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchSubgroups());
	}
}
