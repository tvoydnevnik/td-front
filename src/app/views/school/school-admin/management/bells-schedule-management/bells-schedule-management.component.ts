import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-bells-schedule-management",
	templateUrl: "./bells-schedule-management.component.html",
	styleUrls: ["./bells-schedule-management.component.scss"],
})
export class BellsScheduleManagementComponent {
	schedules;

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.schedules = new TD.ItemsListDatasource<TD.Types.IBellsSchedule>(
			this.changeRef,
		);
		this.FetchSchedules();
	}

	FetchSchedules(): void {
		this.schoolService.GetBellSchedules().subscribe({
			next: (r) => this.schedules.UpdateData(r.data),
		});
	}

	DeleteSchedule(name: string): void {
		this.dialogService
			.Open("Are you sure you want to delete this schedule?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteBellsScheduleGroup(name)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchSchedules());
	}
}
