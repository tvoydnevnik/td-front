import { Component } from "@angular/core";

@Component({
	selector: "app-management",
	templateUrl: "./school-management.component.html",
	styleUrls: ["./school-management.component.scss"],
})
export class SchoolManagementComponent {
	links = [
		{ label: "About", link: "./about" },
		{ label: "Users", link: "./users" },
		{ label: "Bells schedule", link: "./bells-schedule" },
		{ label: "Forms", link: "./forms" },
		{ label: "Form subgroups", link: "./form-subgroups" },
		{ label: "Lessons", link: "./lessons" },
		{ label: "School years", link: "./school-years" },
		{ label: "Quarters", link: "./quarters" },
		{ label: "Quarter weeks", link: "./quarter-weeks" },
	];

	constructor() {}
}
