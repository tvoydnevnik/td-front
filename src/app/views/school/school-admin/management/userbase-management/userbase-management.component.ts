import { UsersManagementService } from "@services/users-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-userbase-management",
	templateUrl: "./userbase-management.component.html",
	styleUrls: ["./userbase-management.component.scss"],
})
export class UserbaseManagementComponent {
	userbase?: TD.ItemsListDatasource<TD.Types.IUser>;
	userbase_columns = new TD.ItemProps([
		{
			title: "ID",
			key: "id",
		},
		{
			title: "Username",
			key: "username",
		},
		{
			title: "Email",
			key: "email",
		},
		{
			title: "First name",
			key: "first_name",
		},
		{
			title: "Last name",
			key: "last_name",
		},
		{
			title: "Patronymic",
			key: "patronymic",
		},
		{
			title: "Invitational codes",
			location: [{ key: "invitations", isArray: true }, { key: "code" }],
		},
		{
			title: "Privilege level",
			key: "role",
		},
	]);

	constructor(
		private readonly usersService: UsersManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.userbase = new TD.ItemsListDatasource<TD.Types.IUser>(this.changeRef);
		this.FetchUsers();
	}

	FetchUsers(withCodes = true): void {
		this.usersService.GetUserbase(withCodes).subscribe({
			next: (response) => this.userbase?.UpdateData(response.data),
		});
	}

	Invite(id: number): void {
		this.usersService.InviteUser(id).subscribe({
			next: () => this.FetchUsers(),
		});
	}

	DeleteUser(id: number): void {
		this.dialogService
			.Open("Are you sure you want to delete this user?")
			.pipe(
				mergeMap(() => this.usersService.DeleteUser(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchUsers());
	}
}
