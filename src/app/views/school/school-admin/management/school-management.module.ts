import { BellsScheduleManagementComponent } from "./bells-schedule-management/bells-schedule-management.component";
import { FormSubgroupsManagementComponent } from "./form-subgroups-management/form-subgroups-management.component";
import { QuarterWeeksManagementComponent } from "./quarter-weeks-management/quarter-weeks-management.component";
import { SchoolAboutManagementComponent } from "./school-about-management/school-about-management.component";
import { SchoolYearsManagementComponent } from "./school-years-management/school-years-management.component";
import { ManagementWrapperComponent } from "./components/management-wrapper/management-wrapper.component";
import { QuartersManagementComponent } from "./quarters-management/quarters-management.component";
import { UserbaseManagementComponent } from "./userbase-management/userbase-management.component";
import { LessonsManagementComponent } from "./lessons-management/lessons-management.component";
import { FormsManagementComponent } from "./forms-management/forms-management.component";
import { SchoolManagementRoutingModule } from "./school-management.routing.module";
import { SchoolManagementComponent } from "./school-management.component";
import { SharedModule } from "@shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

const COMPONENTS = [
	BellsScheduleManagementComponent,
	FormSubgroupsManagementComponent,
	QuarterWeeksManagementComponent,
	SchoolAboutManagementComponent,
	SchoolYearsManagementComponent,
	QuartersManagementComponent,
	UserbaseManagementComponent,
	LessonsManagementComponent,
	SchoolManagementComponent,
	FormsManagementComponent,
];

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		ReactiveFormsModule,
		SchoolManagementRoutingModule,
		ManagementWrapperComponent,
	],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
})
export class SchoolManagementModule {}
