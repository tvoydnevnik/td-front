import { ChangeDetectorRef, Component, inject, LOCALE_ID } from "@angular/core";
import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { FormControl } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-quarter-weeks-management",
	templateUrl: "./quarter-weeks-management.component.html",
	styleUrls: ["./quarter-weeks-management.component.scss"],
})
export class QuarterWeeksManagementComponent {
	private readonly locale = inject(LOCALE_ID);
	private readonly _DateTransformations = [
		{ pipe: new DatePipe(this.locale), arguments: ["fullDate"] },
	];

	weeksStore?: TD.Types.IQuarterWeek[];
	weeksDataSource;
	week_columns = new TD.ItemProps([
		{ title: "ID", key: "id" },
		{
			title: "Starts",
			key: "start_date",
			transform: this._DateTransformations,
		},
		{
			title: "Ends",
			key: "end_date",
			transform: this._DateTransformations,
		},
		{
			title: "Default from",
			key: "indexing_start",
			transform: this._DateTransformations,
		},
		{
			title: "Default to",
			key: "indexing_end",
			transform: this._DateTransformations,
		},
	]);

	quarters?: TD.Types.IQuarter[];
	quarter_selector = new FormControl<number | undefined>(undefined, {
		nonNullable: true,
	});

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.weeksDataSource = new TD.ItemsListDatasource<TD.Types.IQuarterWeek>(
			this.changeRef,
		);
		this.FetchQuarters();
		this.FetchWeeks();
	}

	FetchWeeks(): void {
		this.schoolService.GetQuarterWeeks().subscribe({
			next: (response) => {
				this.weeksStore = response.data;
				this.UpdateDataSource(this.weeksStore);
			},
		});
	}

	FetchQuarters(): void {
		this.schoolService.GetQuarters().subscribe({
			next: (response) => (this.quarters = response.data),
		});
	}

	UpdateDataSource(data?: any): void {
		this.weeksDataSource.UpdateData(data);
	}

	DeleteWeek(id: number): void {
		this.dialogService
			.Open("Are you sure you want to delete this quarter week?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteQuarterWeek(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchWeeks());
	}

	OnQuarterSelected(evt: { value?: number }): void {
		if (!evt.value) {
			this.UpdateDataSource(this.weeksStore);
			return;
		}

		const quarterWeeks = this.weeksStore?.filter(
			(week) => week.quarter_id === evt.value,
		);
		this.UpdateDataSource(quarterWeeks);
	}
}
