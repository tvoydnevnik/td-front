import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { map } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-school-about-management",
	templateUrl: "./school-about-management.component.html",
	styleUrls: ["./school-about-management.component.scss"],
})
export class SchoolAboutManagementComponent {
	schoolInfoForm = this.fb.group({
		name: this.fb.nonNullable.control<string>("", Validators.required),
		address: this.fb.nonNullable.control<string>(""),
		contacts: this.fb.group({
			email: this.fb.nonNullable.control<string>("", Validators.email),
		}),
		headmaster: this.fb.nonNullable.control<TD.Types.IHeadmaster | undefined>(
			undefined,
			Validators.required,
		),
	});

	headmasterSelectConfig?: TD.ItemSelectAutocompleteConfig<TD.Types.IHeadmaster>;

	constructor(
		private readonly fb: FormBuilder,
		private readonly schoolService: SchoolManagementService,
		private readonly snackbar: MatSnackBar,
		private readonly changeRef: ChangeDetectorRef,
	) {
		this.FetchSchoolInfo();
	}

	FetchSchoolInfo(): void {
		this.schoolService.GetInfo().subscribe({
			next: (response) => {
				this.schoolInfoForm.patchValue(response.data!);
				this.headmasterSelectConfig = new TD.ItemSelectAutocompleteConfig({
					label: "Headmaster",
					options: this.schoolService
						.GetHeadmasters()
						.pipe(map((response) => response.data!)),
					initialValue: response.data?.headmaster,
					formControlName: "headmaster",
					displayWith: TD.HeadmasterDisplay,
				});
				this.changeRef.detectChanges();
			},
		});
	}

	UpdateSchool(): void {
		const payload = this.schoolInfoForm.getRawValue();
		let headmaster: number | null | undefined = payload.headmaster?.id;
		if (payload.headmaster === null) headmaster = null;

		this.schoolService
			.UpdateSchoolInfo({
				name: payload.name,
				address: payload.address,
				contacts: payload.contacts,
				headmaster: headmaster === null ? null : { id: headmaster },
			})
			.pipe(HandleCRUDRequest(this.snackbar, true))
			.subscribe(() => this.FetchSchoolInfo());
	}
}
