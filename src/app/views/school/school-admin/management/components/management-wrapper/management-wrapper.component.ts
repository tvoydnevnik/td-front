import { MatDrawerMode, MatSidenavModule } from "@angular/material/sidenav";
import { MatButtonModule } from "@angular/material/button";
import { BreakpointObserver } from "@angular/cdk/layout";
import { MatIconModule } from "@angular/material/icon";
import { Component, Input } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

@Component({
	standalone: true,
	imports: [
		CommonModule,
		MatSidenavModule,
		RouterModule,
		MatButtonModule,
		MatIconModule,
	],
	selector: "app-management-wrapper",
	templateUrl: "./management-wrapper.component.html",
	styleUrls: ["./management-wrapper.component.scss"],
})
export class ManagementWrapperComponent {
	@Input() links?: { label: string; link: string }[];
	sidenavMode: MatDrawerMode = "side";
	sidenavOpened = true;

	constructor(bpo: BreakpointObserver) {
		bpo.observe("(max-width: 620px)").subscribe({
			next: (state) => {
				this.sidenavMode = state.matches ? "over" : "side";
				this.sidenavOpened = !state.matches;
			},
		});
	}
}
