import { ChangeDetectorRef, Component, inject, LOCALE_ID } from "@angular/core";
import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ConfirmationDialogService } from "@td-lib";
import { DatePipe } from "@angular/common";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-school-years-management",
	templateUrl: "./school-years-management.component.html",
	styleUrls: ["./school-years-management.component.scss"],
})
export class SchoolYearsManagementComponent {
	private readonly locale = inject(LOCALE_ID);
	private readonly _DateTransformations = [
		{ pipe: new DatePipe(this.locale), arguments: ["fullDate"] },
	];

	years;
	years_columns = new TD.ItemProps([
		{
			title: "ID",
			key: "id",
		},
		{
			title: "Starts",
			key: "start_date",
			transform: this._DateTransformations,
		},
		{
			title: "Ends",
			key: "end_date",
			transform: this._DateTransformations,
		},
		{
			title: "Active",
			key: "active",
		},
	]);

	constructor(
		private readonly schoolService: SchoolManagementService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly dialogService: ConfirmationDialogService,
		private readonly snackbar: MatSnackBar,
	) {
		this.years = new TD.ItemsListDatasource<TD.Types.ISchoolYear>(
			this.changeRef,
		);
		this.FetchYears();
	}

	FetchYears(): void {
		this.schoolService.GetSchoolYears().subscribe({
			next: (response) => this.years.UpdateData(response.data),
		});
	}

	Supersede(id: number): void {
		this.dialogService
			.Open(
				"Are you sure you want to supersede current active school year with the selected one?",
			)
			.pipe(
				mergeMap(() => this.schoolService.Supersede(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchYears());
	}

	DeleteYear(id: number): void {
		this.dialogService
			.Open("Are you sure you want to delete this school year?")
			.pipe(
				mergeMap(() => this.schoolService.DeleteSchoolYear(id)),
				HandleCRUDRequest(this.snackbar, true),
			)
			.subscribe(() => this.FetchYears());
	}
}
