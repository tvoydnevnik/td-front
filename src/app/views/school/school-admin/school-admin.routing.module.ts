import { EditBellsScheduleComponent } from "./edit/edit-bells-schedule/edit-bells-schedule.component";
import { EditFormSubgroupComponent } from "./edit/edit-form-subgroup/edit-form-subgroup.component";
import { NewBellsScheduleComponent } from "./new/new-bells-schedule/new-bells-schedule.component";
import { NewFormSubgroupComponent } from "./new/new-form-subgroup/new-form-subgroup.component";
import { EditSchoolYearComponent } from "./edit/edit-school-year/edit-school-year.component";
import { NewSchoolYearComponent } from "./new/new-school-year/new-school-year.component";
import { SchoolManagementComponent } from "./management/school-management.component";
import { EditLessonComponent } from "./edit/edit-lesson/edit-lesson.component";
import { NewQuarterComponent } from "./new/new-quarter/new-quarter.component";
import { NewLessonComponent } from "./new/new-lesson/new-lesson.component";
import { EditFormComponent } from "./edit/edit-form/edit-form.component";
import { EditUserComponent } from "./edit/edit-user/edit-user.component";
import { NewFormComponent } from "./new/new-form/new-form.component";
import { NewUserComponent } from "./new/new-user/new-user.component";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { TD } from "@td";

const routes: Routes = [
	{
		path: "management",
		component: SchoolManagementComponent,
		...TD.RBAC_Route(TD.Types.UserRole.ADMIN),
		loadChildren: () =>
			import("./management/school-management.module").then(
				(m) => m.SchoolManagementModule,
			),
	},
	{
		path: "new",
		...TD.RBAC_Route(TD.Types.UserRole.ADMIN),
		children: [
			{
				path: "school-year",
				component: NewSchoolYearComponent,
			},
			{
				path: "quarter",
				component: NewQuarterComponent,
			},
			{
				path: "form",
				component: NewFormComponent,
			},
			{
				path: "form-subgroup",
				component: NewFormSubgroupComponent,
			},
			{
				path: "lesson",
				component: NewLessonComponent,
			},
			{
				path: "user",
				component: NewUserComponent,
			},
			{
				path: "bells-schedule",
				component: NewBellsScheduleComponent,
			},
		],
	},
	{
		path: "edit",
		...TD.RBAC_Route(TD.Types.UserRole.ADMIN),
		children: [
			{
				path: "form/:id",
				component: EditFormComponent,
			},
			{
				path: "form-subgroup/:id",
				component: EditFormSubgroupComponent,
			},
			{
				path: "lesson/:id",
				component: EditLessonComponent,
			},
			{
				path: "user/:id",
				component: EditUserComponent,
			},
			{
				path: "bells-schedule/:id",
				component: EditBellsScheduleComponent,
			},
			{
				path: "school-year/:id",
				component: EditSchoolYearComponent,
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SchoolAdminRoutingModule {}
