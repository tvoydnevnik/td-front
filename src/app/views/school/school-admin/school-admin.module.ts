import { EditSchoolYearWizardComponent } from "./edit/edit-school-year/components/edit-school-year-wizard/edit-school-year-wizard.component";
import { BasicEditSchoolYearComponent } from "./edit/edit-school-year/components/basic-edit-school-year/basic-edit-school-year.component";
import { SchoolYearWizardComponent } from "./new/new-school-year/components/school-year-wizard/school-year-wizard.component";
import { BasicSchoolYearComponent } from "./new/new-school-year/components/basic-school-year/basic-school-year.component";
import { EditBellsScheduleComponent } from "./edit/edit-bells-schedule/edit-bells-schedule.component";
import { EditFormSubgroupComponent } from "./edit/edit-form-subgroup/edit-form-subgroup.component";
import { NewBellsScheduleComponent } from "./new/new-bells-schedule/new-bells-schedule.component";
import { NewFormSubgroupComponent } from "./new/new-form-subgroup/new-form-subgroup.component";
import { EditSchoolYearComponent } from "./edit/edit-school-year/edit-school-year.component";
import { NewSchoolYearComponent } from "./new/new-school-year/new-school-year.component";
import { EditLessonComponent } from "./edit/edit-lesson/edit-lesson.component";
import { NewQuarterComponent } from "./new/new-quarter/new-quarter.component";
import { NewLessonComponent } from "./new/new-lesson/new-lesson.component";
import { EditFormComponent } from "./edit/edit-form/edit-form.component";
import { EditUserComponent } from "./edit/edit-user/edit-user.component";
import { SchoolAdminRoutingModule } from "./school-admin.routing.module";
import { NewUserComponent } from "./new/new-user/new-user.component";
import { NewFormComponent } from "./new/new-form/new-form.component";
import { SharedModule } from "@shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

const COMPONENTS = [
	EditBellsScheduleComponent,
	NewBellsScheduleComponent,
	EditFormSubgroupComponent,
	NewFormSubgroupComponent,
	EditSchoolYearComponent,
	NewSchoolYearComponent,
	EditLessonComponent,
	NewQuarterComponent,
	NewLessonComponent,
	EditFormComponent,
	EditUserComponent,
	NewUserComponent,
	NewFormComponent,
];

@NgModule({
	imports: [
		CommonModule,
		SchoolAdminRoutingModule,
		SharedModule,
		ReactiveFormsModule,
		BasicSchoolYearComponent,
		SchoolYearWizardComponent,
		BasicEditSchoolYearComponent,
		EditSchoolYearWizardComponent,
	],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
})
export class SchoolAdminModule {}
