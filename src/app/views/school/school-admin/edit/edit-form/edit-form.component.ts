import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { map, mergeMap, takeUntil } from "rxjs";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
} from "@angular/core";
import { TD } from "@td";

@Component({
	selector: "app-edit-form",
	templateUrl: "./edit-form.component.html",
	styleUrls: ["./edit-form.component.scss"],
})
export class EditFormComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly changeRef = inject(ChangeDetectorRef);

	formEditGroup = this.fb.group({
		grade: this.fb.nonNullable.control<number | undefined>(
			undefined,
			Validators.required,
		),
		letter: this.fb.nonNullable.control<string>("", Validators.required),
		graduated: this.fb.nonNullable.control<boolean>(false, Validators.required),
		master: this.fb.nonNullable.control<TD.Types.ITeacher | undefined>(
			undefined,
			Validators.required,
		),
		students: this.fb.nonNullable.control<TD.Types.IStudent[] | undefined>(
			undefined,
		),
	});

	masterSelectConfig?: TD.ItemSelectAutocompleteConfig<TD.Types.ITeacher>;
	studentsSelectConfig?: TD.ChipsSelectAutocompleteConfig<TD.Types.IStudent>;

	id?: string | null;

	ngAfterViewInit() {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((map) => {
					this.id = map.get("id");
					return this.schoolService.GetFormById(this.id);
				}),
			)
			.subscribe((response) => {
				const form = response.data!;
				this.formEditGroup.patchValue(form);

				this.masterSelectConfig = new TD.ItemSelectAutocompleteConfig({
					label: "Master",
					options: this.schoolService
						.GetTeachers()
						.pipe(map((response) => response.data!)),
					formControlName: "master",
					initialValue: form.master,
					displayWith: TD.TeacherDisplay,
					required: true,
				});

				this.studentsSelectConfig = new TD.ChipsSelectAutocompleteConfig({
					label: "Students",
					options: this.schoolService
						.GetStudents()
						.pipe(map((response) => response.data!)),
					initialValue: form.students,
					formControlName: "students",
					compareWith: TD.StudentCompare,
					displayWith: TD.StudentDisplay,
					chipDisplayWith: TD.StudentChipDisplay,
				});

				this.changeRef.detectChanges();
			});
	}

	EditForm(): void {
		const payload = this.formEditGroup.getRawValue();

		this.schoolService
			.UpdateForm(this.id!, {
				grade: payload.grade,
				letter: payload.letter,
				graduated: payload.graduated,
				master: { id: payload.master?.id },
				students: payload.students?.map((student) => ({ id: student.id })),
			})
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
