import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AfterViewInit, Component, inject } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { CommonModule } from "@angular/common";

import { forkJoin, mergeMap, takeUntil } from "rxjs";

import { RouterService } from "@services/router.service";
import { SharedModule } from "@shared/shared.module";

import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { ApiClientService } from "@td/api/api-client.service";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-edit-school-year-wizard",
	templateUrl: "./edit-school-year-wizard.component.html",
	styleUrls: ["./edit-school-year-wizard.component.scss"],
})
export class EditSchoolYearWizardComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly apiClient = inject(ApiClientService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly fb = inject(FormBuilder);

	private id: string | null = null;

	schoolYearForm = this.fb.group({
		start_date: this.fb.nonNullable.control<string>("", Validators.required),
		end_date: this.fb.nonNullable.control<string>("", Validators.required),
		active: this.fb.nonNullable.control<"set_active" | "supersede" | "nothing">(
			"nothing",
			Validators.required,
		),
	});

	quarters = new ArrayLikeMap<FormGroup>();
	quarters_map = new Map<number, string>([
		[1, "First"],
		[2, "Second"],
		[3, "Third"],
		[4, "Fourth"],
	]);

	ngAfterViewInit() {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((m) => {
					this.id = m.get("id");
					return forkJoin({
						year: this.apiClient.Get<TD.Types.ISchoolYear>(
							`/school-years/${this.id}`,
						),
						quarters: this.apiClient.Get<TD.Types.IQuarter[]>(
							`/school-years/${this.id}/quarters`,
						),
					});
				}),
			)
			.subscribe((v) => {
				const { year, quarters } = v;
				if (year.data)
					this.schoolYearForm.patchValue({
						start_date: <any>year.data.start_date,
						end_date: <any>year.data.end_date,
					});

				if (quarters) {
					quarters.data?.forEach((v, i) => {
						const idx = i + 1;
						this.quarters.set(
							idx,
							this.fb.group({
								id: this.fb.control({ value: v.id, disabled: true }),
								name: v.name,
								start_date: v.start_date,
								end_date: v.end_date,
							}),
						);
					});
					this.quarters.first?.controls["start_date"].disable();
					this.quarters.last?.controls["end_date"].disable();
				}
			});

		this.schoolYearForm.valueChanges
			.pipe(takeUntil(this.destroy$))
			.subscribe((v) => {
				this.quarters.first?.patchValue({ start_date: v.start_date });
				this.quarters.last?.patchValue({ end_date: v.end_date });
			});
	}

	Update(): void {
		const payload = this.schoolYearForm.getRawValue();
		this.apiClient
			.Patch<TD.Types.ISchoolYear, TD.Types.IEditSchoolYearWizardPayload>(
				`/school-years/wizard/${this.id}`,
				{
					school_year: {
						start_date: new Date(payload.start_date),
						end_date: new Date(payload.end_date),
						active: payload.active !== "nothing" ? true : undefined,
					},
					quarters: Array.from(this.quarters.values()).map((v) => {
						const quarterData = v.getRawValue();

						return {
							id: quarterData.id,
							name: quarterData.name,
							start_date: quarterData.start_date,
							end_date: quarterData.end_date,
						};
					}),
					supersede: payload.active === "supersede" ? true : undefined,
				},
			)
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}

class ArrayLikeMap<ValueT> extends Map<number, ValueT> {
	private _minIndex = 0;
	private _maxIndex = 0;

	public get maxIndex(): number {
		return this._maxIndex;
	}

	public get minIndex(): number {
		return this._minIndex;
	}

	public get first(): ValueT | undefined {
		return super.get(this.minIndex);
	}

	public get last(): ValueT | undefined {
		return super.get(this.maxIndex);
	}

	override set(key: number, value: ValueT): this {
		super.set(key, value);
		this._updateIndexes();
		return this;
	}

	override delete(key: number): boolean {
		const res = super.delete(key);
		if (res) this._updateIndexes();

		return res;
	}

	private _updateIndexes(): void {
		const indexes = Array.from(this.keys());
		this._minIndex = indexes.reduce((prev, curr) =>
			prev < curr ? prev : curr,
		);
		this._maxIndex = indexes.reduce((prev, curr) =>
			prev > curr ? prev : curr,
		);
	}
}
