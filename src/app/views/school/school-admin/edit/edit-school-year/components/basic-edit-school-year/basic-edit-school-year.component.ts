import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { AfterViewInit, Component, inject } from "@angular/core";
import { ApiClientService } from "@td/api/api-client.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { FormBuilder, Validators } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { ActivatedRoute } from "@angular/router";
import { CommonModule } from "@angular/common";
import { mergeMap, takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-basic-edit-school-year",
	templateUrl: "./basic-edit-school-year.component.html",
	styleUrls: ["./basic-edit-school-year.component.scss"],
})
export class BasicEditSchoolYearComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly apiClient = inject(ApiClientService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly fb = inject(FormBuilder);

	formGroup = this.fb.group({
		active: this.fb.nonNullable.control<boolean>(false),
		start_date: this.fb.nonNullable.control<string>("", Validators.required),
		end_date: this.fb.nonNullable.control<string>("", Validators.required),
	});

	private id: string | null = null;

	ngAfterViewInit() {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((m) => {
					this.id = m.get("id");
					return this.apiClient.Get<TD.Types.ISchoolYear>(
						`/school-years/${this.id}`,
					);
				}),
			)
			.subscribe((v) => {
				if (v.data)
					this.formGroup.patchValue({
						start_date: <any>v.data.start_date,
						end_date: <any>v.data.end_date,
						active: v.data.active,
					});
			});
	}

	EditSchoolYear(): void {
		const payload = this.formGroup.getRawValue();
		this.apiClient
			.Patch<TD.Types.ISchoolYear>(`/school-years/${this.id}`, payload)
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
