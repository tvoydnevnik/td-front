import { Component } from "@angular/core";

@Component({
	selector: "app-edit-school-year",
	templateUrl: "./edit-school-year.component.html",
	styleUrls: ["./edit-school-year.component.scss"],
})
export class EditSchoolYearComponent {}
