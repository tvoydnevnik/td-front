import { UsersManagementService } from "@services/users-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { AfterViewInit, Component, inject } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { ActivatedRoute } from "@angular/router";
import { mergeMap, takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-edit-user",
	templateUrl: "./edit-user.component.html",
	styleUrls: ["./edit-user.component.scss"],
})
export class EditUserComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly userService = inject(UsersManagementService);
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly fb = inject(FormBuilder);

	userForm = this.fb.group({
		first_name: this.fb.nonNullable.control<string>("", Validators.required),
		last_name: this.fb.nonNullable.control<string>("", Validators.required),
		patronymic: this.fb.nonNullable.control<string | undefined>(""),
		role: this.fb.nonNullable.control<TD.Types.UserRole | undefined>(
			undefined,
			Validators.required,
		),
	});

	id?: string | null;

	ngAfterViewInit() {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((params) => {
					this.id = params.get("id");
					return this.userService.GetUserById(this.id);
				}),
			)
			.subscribe({
				next: (response) => this.userForm.patchValue(response.data!),
			});
	}

	Update(): void {
		const payload = this.userForm.getRawValue();
		this.userService
			.UpdateUser(this.id!, payload)
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}

	protected readonly TD = TD;
}
