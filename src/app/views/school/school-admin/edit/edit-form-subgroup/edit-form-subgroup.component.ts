import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { AfterViewInit, Component, inject } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { map, takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-edit-form-subgroup",
	templateUrl: "./edit-form-subgroup.component.html",
	styleUrls: ["./edit-form-subgroup.component.scss"],
})
export class EditFormSubgroupComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	formGroup = this.fb.group({
		name: this.fb.nonNullable.control("", Validators.required),
		students: this.fb.nonNullable.control<TD.Types.IStudent[] | undefined>(
			undefined,
		),
	});

	studentsSelectConfig?: TD.ChipsSelectAutocompleteConfig<TD.Types.IStudent>;

	private id?: string | null;

	ngAfterViewInit() {
		this.activatedRoute.paramMap.subscribe((map) => {
			this.id = map.get("id");
			this._FetchData();
		});
	}

	EditSubgroup(): void {
		const payload = this.formGroup.getRawValue();
		this.schoolService
			.UpdateSubgroup(this.id!, {
				name: payload.name,
				students: payload.students?.map((s) => ({ id: s.id })),
			})
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}

	private _FetchData(): void {
		this.schoolService.GetSubgroupById(this.id!).subscribe((resp) => {
			this.formGroup.patchValue(resp.data!);
			this.studentsSelectConfig = new TD.ChipsSelectAutocompleteConfig({
				label: "Students",
				options: this.schoolService.GetStudents().pipe(map((r) => r.data!)),
				formControlName: "students",
				initialValue: resp.data?.students,
				displayWith: TD.StudentDisplay,
				chipDisplayWith: TD.StudentChipDisplay,
				compareWith: TD.StudentCompare,
			});
		});
	}
}
