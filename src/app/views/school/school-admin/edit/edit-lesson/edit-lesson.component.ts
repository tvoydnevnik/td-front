import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { AfterViewInit, Component, inject } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { ActivatedRoute } from "@angular/router";
import { mergeMap, takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-edit-lesson",
	templateUrl: "./edit-lesson.component.html",
	styleUrls: ["./edit-lesson.component.scss"],
})
export class EditLessonComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	lessonEditForm = this.fb.group({
		name: this.fb.nonNullable.control<string>("", Validators.required),
		teachers: this.fb.nonNullable.control<TD.Types.ITeacher[] | undefined>(
			undefined,
		),
	});

	id?: string | null;

	ngAfterViewInit() {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((params) => {
					this.id = params.get("id");
					return this.schoolService.GetLessonById(this.id);
				}),
			)
			.subscribe((response) => this.lessonEditForm.patchValue(response.data!));
	}

	EditLesson(): void {
		const payload = this.lessonEditForm.getRawValue();
		this.schoolService
			.UpdateLesson(this.id!, payload)
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
