import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { LUXON_ISO_TIME_OPTS } from "@td/constants/injection-tokens";
import { AfterViewInit, Component, inject } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { ActivatedRoute } from "@angular/router";
import { mergeMap, takeUntil } from "rxjs";
import { DateTime } from "luxon";
import {
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";
import { TD } from "@td";

@Component({
	selector: "app-edit-bells-schedule",
	templateUrl: "./edit-bells-schedule.component.html",
	styleUrls: ["./edit-bells-schedule.component.scss"],
})
export class EditBellsScheduleComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly activatedRoute = inject(ActivatedRoute);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly fb = inject(FormBuilder);

	private readonly iso_time_opts = inject(LUXON_ISO_TIME_OPTS);

	formGroup = this.fb.group({
		name: this.fb.nonNullable.control<string>("", Validators.required),
		schedule: this.fb.nonNullable.array<
			FormGroup<{
				lesson_index: FormControl<number>;
				start_time: FormControl<string>;
				end_time: FormControl<string>;
			}>
		>([], Validators.required),
	});

	id?: string | null;

	ngAfterViewInit() {
		this.activatedRoute.paramMap
			.pipe(
				mergeMap((map) => {
					this.id = map.get("id");
					return this.schoolService.GetBellsSchedule(this.id);
				}),
			)
			.subscribe((r) => {
				const s = r.data!;
				this.formGroup.patchValue({
					name: s.name,
				});

				s.schedule.forEach((v: TD.Types.ISchedule) =>
					this.formGroup.controls.schedule.push(
						this.fb.group({
							lesson_index: this.fb.nonNullable.control(
								{ value: v.lesson_index, disabled: true },
								Validators.required,
							),
							start_time: this.fb.nonNullable.control(
								DateTime.fromISO(<string>v.start_time).toISOTime(
									this.iso_time_opts,
								) ?? "",
								Validators.required,
							),
							end_time: this.fb.nonNullable.control(
								DateTime.fromISO(<string>v.end_time).toISOTime(
									this.iso_time_opts,
								) ?? "",
								Validators.required,
							),
						}),
					),
				);
			});
	}

	EditSchedule(): void {
		const payload = this.formGroup.getRawValue();
		this.schoolService
			.UpdateBellsSchedule(this.id!, {
				name: payload.name,
				schedule: payload.schedule.map((v) => ({
					lesson_index: v.lesson_index,
					start_time:
						DateTime.fromISO(v.start_time).toISOTime(this.iso_time_opts) ?? "",
					end_time:
						DateTime.fromISO(v.end_time).toISOTime(this.iso_time_opts) ?? "",
				})),
			})
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}

	AddLesson(): void {
		const lessons = this.formGroup.controls.schedule;
		lessons.push(
			this.fb.group({
				lesson_index: this.fb.nonNullable.control<number>(
					{ value: lessons.length + 1, disabled: true },
					Validators.required,
				),
				start_time: this.fb.nonNullable.control<string>(
					"",
					Validators.required,
				),
				end_time: this.fb.nonNullable.control<string>("", Validators.required),
			}),
		);
	}

	DeleteLesson(index: number): void {
		const lessons = this.formGroup.controls.schedule;
		lessons.removeAt(index);
	}
}
