import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { Component, inject } from "@angular/core";
import { map, takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-new-form",
	templateUrl: "./new-form.component.html",
	styleUrls: ["./new-form.component.scss"],
})
export class NewFormComponent extends ReactiveComponent {
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	formGroup = this.fb.group({
		grade: this.fb.nonNullable.control<number | undefined>(
			undefined,
			Validators.required,
		),
		letter: this.fb.nonNullable.control<string>("", Validators.required),
		graduated: this.fb.nonNullable.control<boolean>(false),
		master: this.fb.nonNullable.control<TD.Types.ITeacher | undefined>(
			undefined,
			Validators.required,
		),
		students: this.fb.nonNullable.control<TD.Types.IStudent[] | undefined>(
			undefined,
		),
	});

	masterSelectConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Master",
		options: this.schoolService
			.GetTeachers()
			.pipe(map((r) => r.data?.filter((t) => t.master_of === null) ?? [])),
		formControlName: "master",
		displayWith: TD.TeacherDisplay,
		required: true,
	});
	studentsSelectConfig = new TD.ChipsSelectAutocompleteConfig({
		label: "Students",
		options: this.schoolService
			.GetStudents()
			.pipe(map((response) => response.data!)),
		formControlName: "students",
		compareWith: TD.StudentCompare,
		displayWith: TD.StudentDisplay,
		chipDisplayWith: TD.StudentChipDisplay,
	});

	CreateForm(): void {
		const payload = this.formGroup.getRawValue();
		this.schoolService
			.CreateForm({
				grade: payload.grade,
				letter: payload.letter,
				graduated: payload.graduated,
				master: { id: payload.master?.id },
				students: payload.students?.map((student) => ({ id: student.id })),
			})
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
