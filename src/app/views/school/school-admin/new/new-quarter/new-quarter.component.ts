import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { AfterViewInit, Component, inject } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-new-quarter",
	templateUrl: "./new-quarter.component.html",
	styleUrls: ["./new-quarter.component.scss"],
})
export class NewQuarterComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	public readonly schoolService = inject(SchoolManagementService);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	school_years?: TD.Types.ISchoolYear[];
	quarterForm = this.fb.group({
		name: this.fb.nonNullable.control<string>("", Validators.required),
		school_year_id: this.fb.nonNullable.control<number | undefined>(
			undefined,
			Validators.required,
		),
		start_date: this.fb.nonNullable.control<string>("", Validators.required),
		end_date: this.fb.nonNullable.control<string>("", Validators.required),
		indexing_start: this.fb.nonNullable.control<string>(
			"",
			Validators.required,
		),
		indexing_end: this.fb.nonNullable.control<string>("", Validators.required),
	});
	creationError?: string;

	ngAfterViewInit() {
		this.schoolService.GetSchoolYears().subscribe({
			next: (response) => (this.school_years = response.data),
		});
	}

	Create(): void {
		const payload = this.quarterForm.getRawValue();
		this.schoolService
			.CreateQuarter({
				name: payload.name,
				start_date: new Date(payload.start_date),
				end_date: new Date(payload.end_date),
				indexing_start: new Date(payload.indexing_start),
				indexing_end: new Date(payload.indexing_end),
				school_year: { id: payload.school_year_id },
			})
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
