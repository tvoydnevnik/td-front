import { SchoolManagementService } from "@services/school-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { Component, inject } from "@angular/core";
import { takeUntil } from "rxjs";

@Component({
	selector: "app-new-lesson",
	templateUrl: "./new-lesson.component.html",
	styleUrls: ["./new-lesson.component.scss"],
})
export class NewLessonComponent extends ReactiveComponent {
	// Injected props
	public readonly schoolService = inject(SchoolManagementService);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	lessonForm = this.fb.group({
		name: this.fb.nonNullable.control<string>("", Validators.required),
		stayOnPage: this.fb.nonNullable.control<boolean>(false),
	});

	Create(): void {
		const payload = this.lessonForm.getRawValue();
		this.schoolService
			.CreateLesson(payload.name)
			.pipe(
				HandleCRUDRequest(this.snackbar, payload.stayOnPage),
				takeUntil(this.destroy$),
			)
			.subscribe(() =>
				payload.stayOnPage ? true : this.router.NavigateBack(),
			);
	}
}
