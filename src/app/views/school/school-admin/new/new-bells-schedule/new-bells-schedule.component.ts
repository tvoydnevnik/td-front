import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { LUXON_ISO_TIME_OPTS } from "@td/constants/injection-tokens";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { Component, inject } from "@angular/core";
import { takeUntil } from "rxjs";
import { DateTime } from "luxon";
import {
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";

@Component({
	selector: "app-new-bells-schedule",
	templateUrl: "./new-bells-schedule.component.html",
	styleUrls: ["./new-bells-schedule.component.scss"],
})
export class NewBellsScheduleComponent extends ReactiveComponent {
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly fb = inject(FormBuilder);

	private iso_time_opts = inject(LUXON_ISO_TIME_OPTS);

	formGroup = this.fb.group({
		name: this.fb.nonNullable.control<string>("", Validators.required),
		schedule: this.fb.nonNullable.array<
			FormGroup<{
				lesson_index: FormControl<number>;
				start_time: FormControl<string>;
				end_time: FormControl<string>;
			}>
		>([], Validators.required),
		stayOnPage: this.fb.nonNullable.control<boolean>(false),
	});

	AddLesson(): void {
		const lessons = this.formGroup.controls.schedule;
		lessons.push(
			this.fb.group({
				lesson_index: this.fb.nonNullable.control<number>(
					{ value: lessons.length + 1, disabled: true },
					Validators.required,
				),
				start_time: this.fb.nonNullable.control<string>(
					"",
					Validators.required,
				),
				end_time: this.fb.nonNullable.control<string>("", Validators.required),
			}),
		);
	}

	DeleteLesson(index: number): void {
		const lessons = this.formGroup.controls.schedule;
		lessons.removeAt(index);
	}

	CreateSchedule(): void {
		const payload = this.formGroup.getRawValue();
		this.schoolService
			.AddBellsSchedule({
				name: payload.name,
				schedule: payload.schedule.map((s) => ({
					lesson_index: s.lesson_index,
					start_time:
						DateTime.fromISO(s.start_time).toISOTime(this.iso_time_opts) ?? "",
					end_time:
						DateTime.fromISO(s.end_time).toISOTime(this.iso_time_opts) ?? "",
				})),
			})
			.pipe(
				HandleCRUDRequest(this.snackbar, payload.stayOnPage),
				takeUntil(this.destroy$),
			)
			.subscribe(() =>
				payload.stayOnPage ? undefined : this.router.NavigateBack(),
			);
	}
}
