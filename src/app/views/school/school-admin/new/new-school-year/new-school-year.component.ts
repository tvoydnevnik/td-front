import { Component } from "@angular/core";

@Component({
	selector: "app-new-school-year",
	templateUrl: "./new-school-year.component.html",
	styleUrls: ["./new-school-year.component.scss"],
})
export class NewSchoolYearComponent {}
