import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { CommonModule } from "@angular/common";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
} from "@angular/core";

import { takeUntil } from "rxjs";
import { DateTime } from "luxon";

import { RouterService } from "@services/router.service";
import { SharedModule } from "@shared/shared.module";

import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ApiClientService } from "@td/api/api-client.service";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-school-year-wizard",
	templateUrl: "./school-year-wizard.component.html",
	styleUrls: ["./school-year-wizard.component.scss"],
})
export class SchoolYearWizardComponent
	extends ReactiveComponent
	implements AfterViewInit
{
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);
	private readonly fb = inject(FormBuilder);

	schoolYearForm = this.fb.group({
		start_date: this.fb.nonNullable.control<string>("", Validators.required),
		end_date: this.fb.nonNullable.control<string>("", Validators.required),
		active: this.fb.nonNullable.control<"set_active" | "supersede" | "nothing">(
			"nothing",
			Validators.required,
		),
	});

	quarters = new Map<number, FormGroup>();
	quarters_map = new Map<number, string>([
		[1, "First"],
		[2, "Second"],
		[3, "Third"],
		[4, "Fourth"],
	]);

	ngAfterViewInit() {
		for (const i of this.quarters_map.keys()) {
			this.quarters.set(
				i,
				this.fb.group({
					name: this.fb.nonNullable.control<string>(
						`Quarter ${i}`,
						Validators.required,
					),
					start_date: this.fb.nonNullable.control<string>(
						{ value: "", disabled: i === 1 },
						Validators.required,
					),
					end_date: this.fb.nonNullable.control<string>(
						{ value: "", disabled: i === 4 },
						Validators.required,
					),
				}),
			);
		}

		this.changeRef.detectChanges();

		const { start_date, end_date } = this.schoolYearForm.controls;
		start_date.valueChanges.subscribe((value) =>
			this.quarters.get(1)!.patchValue({ start_date: value }),
		);
		end_date.valueChanges.subscribe((value) =>
			this.quarters.get(4)!.patchValue({ end_date: value }),
		);
	}

	Create(): void {
		const payload = this.schoolYearForm.getRawValue();
		this.apiClient
			.Post<boolean, TD.Types.ISchoolYearWizardPayload>(
				"/school-years/wizard",
				{
					school_year: {
						start_date: new Date(payload.start_date),
						end_date: new Date(payload.end_date),
						active: payload.active !== "nothing" ? true : undefined,
					},
					quarters: Array.from(this.quarters.values()).map((v) => {
						const quarterData = v.getRawValue();

						return {
							name: quarterData.name,
							start_date: <any>(
								(DateTime.fromISO(quarterData.start_date).toISODate() ?? "")
							),
							end_date: <any>(
								(DateTime.fromISO(quarterData.end_date).toISODate() ?? "")
							),
						};
					}),
					supersede: payload.active === "supersede" ? true : undefined,
				},
			)
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
