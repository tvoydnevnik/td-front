import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { ApiClientService } from "@td/api/api-client.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { SharedModule } from "@shared/shared.module";
import { Component, inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-basic-school-year",
	templateUrl: "./basic-school-year.component.html",
	styleUrls: ["./basic-school-year.component.scss"],
})
export class BasicSchoolYearComponent extends ReactiveComponent {
	// Injected props
	private readonly fb = inject(FormBuilder).nonNullable;
	private readonly apiClient = inject(ApiClientService);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	schoolYearForm = this.fb.group({
		start_date: this.fb.control<string>("", Validators.required),
		end_date: this.fb.control<string>("", Validators.required),
		active: this.fb.control<boolean>(false),
	});

	Create(): void {
		const payload = this.schoolYearForm.getRawValue();
		this.apiClient
			.Post<TD.Types.ISchoolYear, Omit<TD.Types.ISchoolYear, "id">>(
				"/school-years",
				{
					start_date: <any>payload.start_date,
					end_date: <any>payload.end_date,
					active: payload.active,
				},
			)
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
