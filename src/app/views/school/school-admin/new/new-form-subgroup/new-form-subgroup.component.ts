import { SchoolManagementService } from "@services/school-management.service";
import { ReactiveComponent } from "@td/interfaces/reactive.component";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { Component, inject } from "@angular/core";
import { map, takeUntil } from "rxjs";
import { TD } from "@td";

@Component({
	selector: "app-new-form-subgroup",
	templateUrl: "./new-form-subgroup.component.html",
	styleUrls: ["./new-form-subgroup.component.scss"],
})
export class NewFormSubgroupComponent extends ReactiveComponent {
	// Injected props
	private readonly schoolService = inject(SchoolManagementService);
	private readonly fb = inject(FormBuilder);
	private readonly snackbar = inject(MatSnackBar);
	private readonly router = inject(RouterService);

	formGroup = this.fb.group({
		name: this.fb.nonNullable.control("", Validators.required),
		form: this.fb.nonNullable.control<TD.Types.IForm | undefined>(
			undefined,
			Validators.required,
		),
		students: this.fb.nonNullable.control<TD.Types.IStudent[] | undefined>(
			undefined,
		),
	});

	formSelectConfig = new TD.ItemSelectAutocompleteConfig({
		label: "Form",
		options: this.schoolService
			.GetForms()
			.pipe(map((response) => response.data!)),
		formControlName: "form",
		displayWith: TD.FormDisplay,
		required: true,
	});
	studentsSelectConfig = new TD.ChipsSelectAutocompleteConfig({
		label: "Students",
		options: [],
		formControlName: "students",
		compareWith: TD.StudentCompare,
		displayWith: TD.StudentDisplay,
		chipDisplayWith: TD.StudentChipDisplay,
	});

	FormSelected(v?: TD.Types.IForm): void {
		if (v) this.studentsSelectConfig.SetOptions(v.students || []);
	}

	CreateSubgroup(): void {
		const payload = this.formGroup.getRawValue();
		this.schoolService
			.CreateSubgroup({
				name: payload.name,
				form: { id: payload.form!.id },
				students: payload.students?.map((s) => ({ id: s.id })),
			})
			.pipe(HandleCRUDRequest(this.snackbar), takeUntil(this.destroy$))
			.subscribe(() => this.router.NavigateBack());
	}
}
