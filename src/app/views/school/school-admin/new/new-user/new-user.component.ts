import { UsersManagementService } from "@services/users-management.service";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { Component } from "@angular/core";
import { TD } from "@td";

@Component({
	selector: "app-new-user",
	templateUrl: "./new-user.component.html",
	styleUrls: ["./new-user.component.scss"],
})
export class NewUserComponent {
	userForm = this.fb.group({
		first_name: this.fb.nonNullable.control<string>("", Validators.required),
		last_name: this.fb.nonNullable.control<string>("", Validators.required),
		patronymic: this.fb.nonNullable.control<string>(""),
		role: this.fb.nonNullable.control<TD.Types.UserRole | undefined>(
			undefined,
			Validators.required,
		),
		stayOnPage: this.fb.nonNullable.control<boolean>(false),
	});

	creationError?: string;

	constructor(
		private readonly fb: FormBuilder,
		private readonly usersService: UsersManagementService,
		private readonly snackbar: MatSnackBar,
		private readonly router: RouterService,
	) {}

	Create(): void {
		const payload = this.userForm.getRawValue();
		this.usersService
			.PreRegisterUser(payload)
			.pipe(HandleCRUDRequest(this.snackbar, payload.stayOnPage))
			.subscribe(() =>
				payload.stayOnPage ? true : this.router.NavigateBack(),
			);
	}

	protected readonly TD = TD;
	protected readonly Object = Object;
}
