import { inject } from "@angular/core";
import { CanActivateFn, CanMatchFn, Router } from "@angular/router";

export const easterEggGuardFn: CanActivateFn | CanMatchFn = () => {
	const router = inject(Router);

	const navigationState = router.getCurrentNavigation()?.extras.state;
	return navigationState ? navigationState["allowed"] : router.parseUrl("/");
};
