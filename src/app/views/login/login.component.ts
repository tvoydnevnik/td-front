import { IHttpError } from "@td/interfaces/http-error.interface";
import { LoadingComponent, OverlayService } from "@td-lib";
import { FormBuilder, Validators } from "@angular/forms";
import { RouterService } from "@services/router.service";
import { AuthService } from "@services/auth.service";
import { Component } from "@angular/core";
import { tap } from "rxjs";

@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
	loginForm = this.fb.group({
		identifier: this.fb.nonNullable.control("", Validators.required),
		password: this.fb.nonNullable.control("", Validators.required),
	});
	loginError?: string;
	public showPassword = false;

	constructor(
		private readonly authService: AuthService,
		private readonly fb: FormBuilder,
		private readonly router: RouterService,
		private readonly overlayService: OverlayService,
	) {}

	login(): void {
		const payload = this.loginForm.getRawValue();

		this.overlayService.createOverlay(LoadingComponent);
		this.authService
			.Login(payload.identifier, payload.password)
			.pipe(tap(() => this.overlayService.clearOverlay()))
			.subscribe({
				next: () => {
					this.router.NavigateBack();
				},
				error: (err: IHttpError) => {
					this.overlayService.clearOverlay();
					this.loginError = err.error.message;
				},
			});
	}
}
