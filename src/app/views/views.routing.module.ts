import { EasterEggComponent } from "./easter-egg/easter-egg.component";
import { easterEggGuardFn } from "./easter-egg/guards/easter-egg.guard";
import { TimetableComponent } from "./timetable/timetable.component";
import { RegisterComponent } from "./register/register.component";
import { ProfileComponent } from "./profile/profile.component";
import { noAuthGuardFn } from "@shared/guards/no-auth.guard";
import { LoginComponent } from "./login/login.component";
import { DiaryComponent } from "./diary/diary.component";
import { RouterModule, Routes } from "@angular/router";
import { authGuardFn } from "@shared/guards/auth.guard";
import { HomeComponent } from "./home/home.component";
import { NgModule } from "@angular/core";

const routes: Routes = [
	{
		path: "home",
		component: HomeComponent,
	},
	{
		path: "timetable",
		component: TimetableComponent,
		canActivate: [authGuardFn],
	},
	{
		path: "diary",
		component: DiaryComponent,
		canActivate: [authGuardFn],
	},
	{
		path: "login",
		component: LoginComponent,
		canActivate: [noAuthGuardFn],
	},
	{
		path: "register",
		component: RegisterComponent,
		canActivate: [noAuthGuardFn],
	},
	{
		path: "profile",
		component: ProfileComponent,
		canActivate: [authGuardFn],
	},
	{
		path: "easteregg",
		component: EasterEggComponent,
		canActivate: [easterEggGuardFn],
	},
	{
		path: "school",
		loadChildren: () =>
			import("./school/school.module").then((m) => m.SchoolModule),
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ViewsRoutingModule {}
