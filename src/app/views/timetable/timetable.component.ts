import { ChangeDetectorRef, Component, inject } from "@angular/core";
import { ApiClientService } from "@td/api/api-client.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TD } from "@td";

@Component({
	selector: "app-timetable",
	templateUrl: "./timetable.component.html",
	styleUrls: ["./timetable.component.scss"],
})
export class TimetableComponent {
	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);
	private readonly snackbar = inject(MatSnackBar);

	timetable?: TD.Types.ITimetableResponse;

	constructor() {
		this.apiClient
			.Get<TD.Types.ITimetableResponse, TD.Types.ITimetableRequest>(
				"/timetable/mine",
				{ request: { request_opts: { for_week: true } } },
			)
			.subscribe({
				next: (response) => {
					this.timetable = response.data;
					this.changeRef.detectChanges();
				},
				error: (response) => {
					this.snackbar.open(response.error.message, "OK");
				},
			});
	}
}
