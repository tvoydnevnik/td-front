import { ChangeDetectorRef, Component, inject, LOCALE_ID } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UsersService } from "@services/users.service";
import { DatePipe } from "@angular/common";
import { TD } from "@td";

@Component({
	selector: "app-profile",
	templateUrl: "./profile.component.html",
	styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent {
	private readonly locale = inject(LOCALE_ID);

	sessions?: TD.ItemsListDatasource<TD.Types.SessionData>;
	sessions_mapping = new TD.ItemProps([
		{
			title: "Web Client",
			location: [{ key: "device_info" }, { key: "client" }],
		},
		{
			title: "Device",
			location: [{ key: "device_info" }, { key: "device" }],
		},
		{
			title: "Operating System",
			location: [{ key: "device_info" }, { key: "os" }],
		},
		{
			title: "Expires",
			location: [{ key: "cookie" }, { key: "expires" }],
			transform: [{ pipe: new DatePipe(this.locale), arguments: ["short"] }],
		},
	]);

	constructor(
		public readonly usersService: UsersService,
		private readonly changeRef: ChangeDetectorRef,
		private readonly snackbar: MatSnackBar,
	) {
		this.sessions = new TD.ItemsListDatasource<TD.Types.SessionData>(
			this.changeRef,
		);
		this.FetchSessions();
	}

	FetchSessions(): void {
		this.usersService.GetMySessions().subscribe({
			next: (response) => this.sessions?.UpdateData(response.data),
		});
	}

	LogoutSession(id: string): void {
		this.usersService.LogoutSession(id).subscribe({
			next: (response) => {
				if (response.data)
					this.snackbar.open("Logged out!", undefined, {
						duration: 2500,
					});

				this.FetchSessions();
			},
		});
	}

	FormatSessionDescription(session: TD.Types.SessionData): string {
		return `
			${session.device_info.client || "Unknown client"},
			${session.device_info.device || "unknown device"} from
			${session.device_info.os || "unknown OS"}
		`;
	}
}
