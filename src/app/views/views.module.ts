import { EasterEggComponent } from "./easter-egg/easter-egg.component";
import { TimetableComponent } from "./timetable/timetable.component";
import { RegisterComponent } from "./register/register.component";
import { ProfileComponent } from "./profile/profile.component";
import { ViewsRoutingModule } from "./views.routing.module";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

const COMPONENTS = [
	LoginComponent,
	RegisterComponent,
	HomeComponent,
	TimetableComponent,
	ProfileComponent,
	EasterEggComponent,
];

@NgModule({
	imports: [
		CommonModule,
		ViewsRoutingModule,
		ReactiveFormsModule,
		SharedModule,
	],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
})
export class ViewsModule {}
