import { FormBuilder, ValidatorFn, Validators } from "@angular/forms";
import { HandleCRUDRequest } from "@td/utils/crud-operations-helpers";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterService } from "@services/router.service";
import { AuthService } from "@services/auth.service";
import { Component } from "@angular/core";

function PasswordsMatch(
	pwd_control: string,
	matching_pwd_control: string,
): ValidatorFn {
	return (control) => {
		return control.get(pwd_control)?.value ===
			control.get(matching_pwd_control)?.value
			? null
			: { mismatch: true };
	};
}

@Component({
	selector: "app-register",
	templateUrl: "./register.component.html",
	styleUrls: ["./register.component.scss"],
})
export class RegisterComponent {
	registrationForm = this.fb.group(
		{
			code: this.fb.nonNullable.control<string>("", Validators.required),
			username: this.fb.nonNullable.control<string>(
				"",
				Validators.required,
			),
			email: this.fb.nonNullable.control<string>("", [
				Validators.email,
				Validators.required,
			]),
			password: this.fb.nonNullable.control<string>(
				"",
				Validators.required,
			),
			password_repeat: this.fb.nonNullable.control<string>(
				"",
				Validators.required,
			),
		},
		{ validators: [PasswordsMatch("password", "password_repeat")] },
	);
	registrationError?: string;

	showPassword = false;

	constructor(
		private readonly fb: FormBuilder,
		private readonly authService: AuthService,
		private readonly snackbar: MatSnackBar,
		private readonly router: RouterService,
	) {}

	register(): void {
		const { code, username, email, password } =
			this.registrationForm.getRawValue();
		this.authService
			.Register({ code, username, email, password })
			.pipe(HandleCRUDRequest(this.snackbar))
			.subscribe({
				next: () => this.router.Navigate("/login"),
			});
	}
}
