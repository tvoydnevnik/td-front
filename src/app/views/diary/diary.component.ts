import { ApiClientService } from "@td/api/api-client.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { PageEvent } from "@angular/material/paginator";
import { SharedModule } from "@shared/shared.module";
import { CommonModule } from "@angular/common";
import { map } from "rxjs";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	inject,
	Input,
} from "@angular/core";
import { TD } from "@td";
import { HandleHTTPError } from "@td/utils/crud-operations-helpers";

@Component({
	standalone: true,
	imports: [CommonModule, SharedModule],
	selector: "app-diary",
	templateUrl: "./diary.component.html",
	styleUrls: ["./diary.component.scss"],
})
export class DiaryComponent implements AfterViewInit {
	// Input props
	@Input() mine = true;
	@Input() student?: TD.Types.IStudent;

	// Injected props
	private readonly changeRef = inject(ChangeDetectorRef);
	private readonly apiClient = inject(ApiClientService);
	private readonly snackbar = inject(MatSnackBar);

	cachedDiaries = new Map<string, TD.Types.ITimetableResponse | undefined>();
	diary?: TD.Types.ITimetableResponse;
	pagination_data?: TD.Types.IQuarterPagination;

	ngAfterViewInit() {
		if (!this.mine && !this.student)
			throw new Error("No student provided to fetch diary for.");

		this.apiClient
			.Get<TD.Types.IQuarterPagination>(`/quarters/current/pagination-data`)
			.pipe(
				map((r) => {
					this.pagination_data = r.data;
					if (!this.pagination_data) return;

					return this.FetchDiary(<any>{
						pageIndex: this.pagination_data?.current_week,
					});
				}),
			)
			.subscribe();
	}

	FetchDiary(e: PageEvent): void {
		const d = this.cachedDiaries.get(`qw${e.pageIndex}`);
		if (d) {
			this.diary = d;
			this.changeRef.detectChanges();
			return;
		}

		this.apiClient
			.Get<TD.Types.ITimetableResponse, TD.Types.ITimetableRequest>(
				this.mine ? "/timetable/mine" : "/timetable",
				{
					request: {
						filtering_opts: {
							quarter_week_index: e.pageIndex,
							student_id: this.mine ? undefined : this.student?.id,
						},
						request_opts: { with_diary: true, for_quarter: true },
					},
				},
			)
			.pipe(HandleHTTPError(this.snackbar))
			.subscribe({
				next: (r) => {
					this.cachedDiaries.set(`qw${e.pageIndex}`, r.data);
					this.diary = r.data;
					this.changeRef.detectChanges();
				},
			});
	}
}
