import { Injectable } from "@angular/core";
import { TD } from "../tvoydnevnik/td";
import { environment } from "@env";

@Injectable({
	providedIn: "root",
})
export class UsersManagementService {
	constructor(private readonly http: TD.HttpService) {}

	GetUserbase(withCodes?: boolean): TD.HttpResponse<TD.Types.IUser[]> {
		const options = withCodes ? { params: { codes: true } } : {};
		return this.http.get<TD.Types.IUser[]>(
			`${environment.apiUrl}/users`,
			options,
		);
	}

	GetUserById(id: string | null): TD.HttpResponse<TD.Types.IUser> {
		return this.http.get<TD.Types.IUser>(`${environment.apiUrl}/users/${id}`);
	}

	PreRegisterUser(payload: {
		first_name: string;
		last_name: string;
		patronymic?: string;
		role?: TD.Types.UserRole;
	}): TD.HttpResponse<boolean> {
		return this.http.post<boolean>(
			`${environment.apiUrl}/auth/pre-register`,
			payload,
		);
	}

	UpdateUser(
		id: string | null,
		payload: {
			first_name: string;
			last_name: string;
			patronymic?: string;
			role?: TD.Types.UserRole;
		},
	): TD.HttpResponse<TD.Types.IUser> {
		return this.http.patch<TD.Types.IUser>(
			`${environment.apiUrl}/users/${id}`,
			payload,
		);
	}

	InviteUser(user: number): TD.HttpResponse<string> {
		return this.http.post<string>(`${environment.apiUrl}/auth/invite`, {
			user,
		});
	}

	DeleteUser(id: number): TD.HttpResponse<boolean> {
		return this.http.delete<boolean>(`${environment.apiUrl}/users/${id}`);
	}
}
