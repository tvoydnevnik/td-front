import { Injectable } from "@angular/core";
import { environment } from "@env";
import { TD } from "@td";

@Injectable({
	providedIn: "root",
})
export class SchoolManagementService {
	constructor(private readonly http: TD.HttpService) {}

	/* School info */
	GetInfo(): TD.HttpResponse<TD.Types.ISchool> {
		return this.http.get(`${environment.apiUrl}/school/about`);
	}

	GetHeadmasters(): TD.HttpResponse<TD.Types.IHeadmaster[]> {
		return this.http.get(`${environment.apiUrl}/headmasters`);
	}

	GetTeachers(): TD.HttpResponse<TD.Types.ITeacher[]> {
		return this.http.get(`${environment.apiUrl}/teachers`);
	}

	UpdateSchoolInfo(
		payload: TD.Types.DeepPartial<TD.Types.ISchool> | { headmaster: null },
	): TD.HttpResponse<TD.Types.ISchool> {
		return this.http.patch(`${environment.apiUrl}/school/update`, payload);
	}

	/* School years */
	GetSchoolYears(): TD.HttpResponse<TD.Types.ISchoolYear[]> {
		return this.http.get(`${environment.apiUrl}/school-years`);
	}
	Supersede(id: number): TD.HttpResponse<boolean> {
		return this.http.post(`${environment.apiUrl}/school-years/supersede`, {
			with: id,
		});
	}

	DeleteSchoolYear(id: number): TD.HttpResponse<boolean> {
		return this.http.delete(`${environment.apiUrl}/school-years/${id}`);
	}

	/* Quarters */
	GetQuarters(): TD.HttpResponse<TD.Types.IQuarter[]> {
		return this.http.get(`${environment.apiUrl}/quarters`);
	}

	CreateQuarter(
		payload: TD.Types.DeepPartial<TD.Types.IQuarter>,
	): TD.HttpResponse<TD.Types.IQuarter> {
		return this.http.post(`${environment.apiUrl}/quarters`, payload);
	}
	DeleteQuarter(id: number): TD.HttpResponse<boolean> {
		return this.http.delete(`${environment.apiUrl}/quarters/${id}`);
	}

	GenerateQuarterWeeks(id: number): TD.HttpResponse<TD.Types.IQuarterWeek[]> {
		return this.http.post(
			`${environment.apiUrl}/quarters/weeks/fill-for-quarter/${id}`,
			null,
		);
	}

	/* Quarter weeks */
	GetQuarterWeeks(): TD.HttpResponse<TD.Types.IQuarterWeek[]> {
		return this.http.get(`${environment.apiUrl}/quarters/weeks`, {
			params: { withQuarters: true },
		});
	}

	DeleteQuarterWeek(id: number): TD.HttpResponse<boolean> {
		return this.http.delete(`${environment.apiUrl}/quarters/weeks/${id}`);
	}

	/* Forms */
	GetForms(): TD.HttpResponse<TD.Types.IForm[]> {
		return this.http.get(`${environment.apiUrl}/forms`);
	}

	GetFormById(id: string | null): TD.HttpResponse<TD.Types.IForm> {
		return this.http.get(`${environment.apiUrl}/forms/${id}`);
	}

	CreateForm(
		payload: TD.Types.DeepPartial<TD.Types.IForm>,
	): TD.HttpResponse<TD.Types.IForm> {
		return this.http.post(`${environment.apiUrl}/forms`, payload);
	}

	UpdateForm(
		id: string | null,
		payload: TD.Types.DeepPartial<TD.Types.IForm>,
	): TD.HttpResponse<TD.Types.IForm> {
		return this.http.patch(`${environment.apiUrl}/forms/${id}`, payload);
	}

	DeleteFrom(id: number): TD.HttpResponse<boolean> {
		return this.http.delete(`${environment.apiUrl}/forms/${id}`);
	}

	/* Students */
	GetStudents(): TD.HttpResponse<TD.Types.IStudent[]> {
		return this.http.get(`${environment.apiUrl}/students`);
	}

	/* Lessons */
	GetLessons(): TD.HttpResponse<TD.Types.ILesson[]> {
		return this.http.get(`${environment.apiUrl}/lessons`);
	}

	GetLessonById(id: string | null): TD.HttpResponse<TD.Types.ILesson> {
		return this.http.get(`${environment.apiUrl}/lessons/${id}`);
	}

	CreateLesson(name: string): TD.HttpResponse<TD.Types.ILesson> {
		return this.http.post(`${environment.apiUrl}/lessons`, { name });
	}

	UpdateLesson(
		id: string | null,
		payload: Partial<TD.Types.ILesson>,
	): TD.HttpResponse<TD.Types.ILesson> {
		return this.http.patch(`${environment.apiUrl}/lessons/${id}`, payload);
	}

	DeleteLesson(id: number): TD.HttpResponse<boolean> {
		return this.http.delete(`${environment.apiUrl}/lessons/${id}`);
	}

	/* Subgroups */
	GetSubgroups(): TD.HttpResponse<TD.Types.IFormSubgroup[]> {
		return this.http.get(`${environment.apiUrl}/forms/subgroups`);
	}
	GetSubgroupById(id: string | null): TD.HttpResponse<TD.Types.IFormSubgroup> {
		return this.http.get(`${environment.apiUrl}/forms/subgroups/${id}`);
	}

	CreateSubgroup(
		payload: TD.Types.DeepPartial<TD.Types.IFormSubgroup>,
	): TD.HttpResponse<TD.Types.IFormSubgroup> {
		return this.http.post(`${environment.apiUrl}/forms/subgroups`, payload);
	}

	UpdateSubgroup(
		id: string | null,
		payload: TD.Types.DeepPartial<TD.Types.IFormSubgroup>,
	): TD.HttpResponse<TD.Types.IFormSubgroup> {
		return this.http.patch(
			`${environment.apiUrl}/forms/subgroups/${id}`,
			payload,
		);
	}

	DeleteSubgroup(id: number): TD.HttpResponse<boolean> {
		return this.http.delete(`${environment.apiUrl}/forms/subgroups/${id}`);
	}

	/* Bells schedule */
	GetBellSchedules(): TD.HttpResponse<TD.Types.IBellsSchedule[]> {
		return this.http.get(`${environment.apiUrl}/school/bell-schedules`);
	}

	GetBellsSchedule(
		name: string | null,
	): TD.HttpResponse<TD.Types.IBellsSchedule> {
		return this.http.get(`${environment.apiUrl}/school/bells-schedule/${name}`);
	}

	AddBellsSchedule(
		payload: TD.Types.IBellsSchedulePayload,
	): TD.HttpResponse<TD.Types.IBellsSchedule[]> {
		return this.http.post(
			`${environment.apiUrl}/school/bells-schedule`,
			payload,
		);
	}

	UpdateBellsSchedule(
		name: string,
		payload: Partial<TD.Types.IBellsSchedulePayload>,
	): TD.HttpResponse<TD.Types.IBellsSchedule> {
		return this.http.patch(
			`${environment.apiUrl}/school/bells-schedule/${name}`,
			payload,
		);
	}

	DeleteBellsScheduleGroup(name: string): TD.HttpResponse<boolean> {
		return this.http.delete(
			`${environment.apiUrl}/school/bells-schedule/${name}`,
		);
	}
}
