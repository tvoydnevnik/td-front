import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { OverlayModule } from "@angular/cdk/overlay";
import { DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";

import { MenuGroupHeaderComponent } from "./wrapper/components/menu-group-header/menu-group-header.component";
import { MenuComponent } from "./wrapper/components/menu/menu.component";
import { WrapperComponent } from "./wrapper/wrapper.component";

import { constantProviders } from "./app.constant-providers";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { appProviders } from "./app.providers";

import { NavigationHistoryService } from "@services/navigation-history.service";
import { SharedModule } from "@shared/shared.module";

@NgModule({
	declarations: [AppComponent, WrapperComponent],
	imports: [
		MenuGroupHeaderComponent,
		BrowserAnimationsModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserModule,
		OverlayModule,
		SharedModule,
		MenuComponent,
		DatePipe,
	],
	providers: [...appProviders, ...constantProviders],
	bootstrap: [AppComponent],
})
export class AppModule {
	constructor(private readonly navigationHistory: NavigationHistoryService) {}
}
