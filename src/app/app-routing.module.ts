import { WrapperComponent } from "./wrapper/wrapper.component";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

const routes: Routes = [
	{
		path: "",
		pathMatch: "full",
		redirectTo: "/home",
	},
	{
		path: "",
		component: WrapperComponent,
		loadChildren: () =>
			import("./views/views.module").then((m) => m.ViewsModule),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
