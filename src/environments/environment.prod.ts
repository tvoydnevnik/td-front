import { IEnvironment } from "./environment.interface";
import * as process from "process";

const API_URL = process.env["API_URL"];
if (!API_URL) {
	throw Error("Api URL not provided via environment!");
}

export const environment: IEnvironment = {
	production: true,
	apiUrl: API_URL,
};
